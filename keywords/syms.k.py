for arg in re.split('\s+', ARGSTRING):
    symmatch = re.match('((?P<ws>[^\.\s]*)\.)?(?P<symname>[^<>\s]*)(<(?P<units>.*)>)?', arg)
    
    if symmatch == None:
        # wrong format
        raise exc('Symbol name is in incorrect format. Type help syms for more information.')
    
    workspace = symmatch.group('ws')
    symname = symmatch.group('symname')
    units = symmatch.group('units')
    
    if workspace == None:
        targetws = puny.DEFAULTWS
    else:
        targetws = workspace.upper()
    
    if not targetws in puny.WORKSPACES.keys():
        raise puny.exc('Workspace '+targetws+' does not exist!')
    
    # go ahead and create the object and insert it, even if there is a conflict
    if units == None:
        dimensions = puny.dimension_dict()
    else:
        # use quantity machinery to do the dimension stuff. slow but easy
        tmpQuant = puny.quantity(string='1<'+units+'>')
        dimensions = tmpQuant.dimensions
        del tmpQuant
    
    newsymbol = puny.symbol(name='',dimensions=dimensions)
    newsymbol.setName(symname)   # add the name while checking for validity
    
    puny.WORKSPACES[targetws].setMember(newsymbol)
