This keyword deletes a workspace and switches to one of the remaining workspaces. If no workspaces exist after deletion, the default workspace is created (empty) and switched to.
