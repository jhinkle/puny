This keyword creates a workspace with the given name and switches to it. Workspace members may be referenced from other workspaces using a dot, e.g. product=3*ws2.factor1
