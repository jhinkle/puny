This function inserts the given named symbolic variables into the current workspace. The declarations should be given in the form name<units>.

For instance if you wish to declare a symbol named f0 with the dimensions of frequency you might type:

>> syms f0<Hz>

Note that only the dimensions are kept so the following would be entirely equivalent:

>>syms f0<MHz>

Also note that symbol (and function) names follow the same conventions as variable names.
