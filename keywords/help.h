This keyword may be used to get detailed information about most facets of \
puny. To use it simply type the word help followed by the specific subject \
you want help with.
