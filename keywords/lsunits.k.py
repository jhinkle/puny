output("Units in database (dimensions in parenthesis)\n")

for k in puny.UNITS.keys():
    u=puny.UNITS[k]
    if len(u.aliases) > 0:
      output(u.abbrev +" ("+",".join(u.aliases)+")"+ "\t" + u.name + " ("+u.dimString()+")")
    else:
      output(u.abbrev + "\t" + u.name + " ("+u.dimString()+")")
