if type(ARGS[0]) == types.StringType:
    REQWS = ARGS[0].upper()

    if REQWS in puny.WORKSPACES.keys():
        # workspace exists so switch to it
        puny.DEFAULTWS = REQWS
        # do this silently
        #output('Default workspace switched to '+REQWS)

    else:
        output('Workspace '+REQWS+' does not exits!')

else:
    output('Workspace name is not in valid format.')
