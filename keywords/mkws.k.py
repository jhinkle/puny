if type(ARGS[0]) == types.StringType:
    REQWS = ARGS[0].upper()
    
    if not re.match('[a-zA-Z][0-9a-zA-Z_]*$', REQWS):
        output('Workspace name is not in valid format.')
    
    elif REQWS in puny.WORKSPACES.keys():
        # workspace exists
        output('Workspace already exists: '+REQWS)
        
    else:
        puny.WORKSPACES[REQWS] = puny.workspace()
        puny.DEFAULTWS = REQWS
    
else:
    output('Workspace name is not in valid format.')
