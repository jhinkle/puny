if len(ARGS) == 0:
    # display generic help message
    output("""For help on a specific subject type help <subject>. Some examples:

help keywords       (Lists available keywords)
help functions      (Lists available functions)
help syntax         (Describes the expected syntax of commands)
help quantity       (Describes how quantities are handled in puny)
help units          (Describes how units are handled in puny)""")

else:
    # specifying a subject
    if ARGS[0] == 'keywords':
        # display list of keywords
        output("The following keywords are available on the system:\n")
        
        all_kw = KEYWORDS.keys() + EXTERNAL_KEYWORDS.keys()
        all_kw.sort()
        
        output("\n".join(all_kw)+"\n\nType help <keyword> for help with a specific keyword.")
    
    elif ARGS[0] == 'functions':
        # display list of functions
        output("The following functions are available on the system:\n")

        all_funcs = pmath.FUNCTIONS.keys()
        all_funcs.sort()
        
        output("\n".join(all_funcs)+"\n\nType help <function> for help with a specific function.")
    
    elif ARGS[0] == 'syntax':
        output("""
Commands in puny come in a few different flavors:

The first type of command is called a keyword. It does not take any arguments in parentheses afterwords, but instead it is sent the remainder of the command line as an argument. In this way, running a keyword is like running a program froma usual shell. It does not return a quantity as results and thus may not be used in formulae. As you view the list of keywords (by issuing the "help" command),this makes sense though, since the keywords are usually used to print information to the screen or perform some function that is more related to the puny environment instead of the quantities in a certain workspace.

The second main type of command is one which may be parsed to represent numerical data. This would include something like "2+3". If you wish to set a variable name other than "ans" to the resulting quantity you may just put it before an equals sign, like "a=2+3". Variable names may only contain alphanumeric characters(A-Za-z) or underscores (_) and must start with a letter.
(Note 1: to find a variable's current value simply type the variable name on a line by itself)
(Note 2: to list all variables use the "who" keyword)

A variable name beginning with an underscore is assumed to be the name of a physical constant, such as _pi or _c. These names are reserved and may not be redefined, but the variables may be used just like normal user-defined variables in expressions.

Within the part of a command which is expected to be executed, the regular arithmetic operators may be used (+-*/^). The regular order of operations is followed for these operations.

Optionally, a trailing vertical pipe character denotes that some function should be performed on the resultant quantity. One such useful function is to convertthe quantity into some specified units. This may be accomplished by appending "|in <units>" to the command.

See "help quantity" for a syntax rules to use when describing variables.

Functions such as sqrt(x) or cos(_pi/2) may be used in any expression. They will all return a quantity as a result unless an error occurs. Arguments to functions are parsed as individual expressions before the functions are executed.
""")
    
    elif ARGS[0] == 'quantity':
        output("""
In puny, each variable represents a physical quantity or collection of quantities. These quantities have units (m/lb/C, etc.) and dimensions (length/mass/charge, etc.) associated with them. However, the purpose of the program is to remove the abstraction of differing unitary systems and the like. The units are simply a way to describe the quantity under inspection to the computer. The programwill then decide how best to convert and digest the quantity as it performs therequested operations, then report the quantity back to the user in whatever units he chooses. This means that when you are confronted with a formula such as U=_G*m1*m2/r you can just input m1, m2 and r in whatever units you have and the result may be automatically converted to whatever units you may request (such as Jin the example above).
(NOTE: For information about how to manipulate units see "help units")

Ordinary scalars may be assigned by simply typing the numerical value, followedby (optionally) the units in carats after. Vectors may be declared in one of two ways:
(NOTE: All elements in a vector must have the same units)

The first way to declare a vector is by declaring each individual element. The elements should be inside square brackets, separated by commas. The following are valid vector declarations:

    [0, 1, -23, 5]
    [3.5, _pi, 5.2<kg>2<m>/3<s^2>]<N>
    [0, inf]

The other way to declare vectors is by defining a range over which elements will be picked at some random interval. The beginning, interval, and end of this range should be separated by colons. If the interval is omitted, it is assumed to be equal to 1 in the units requested. So these are valid vector declarations as well:

    0 : .01 : _pi
    1:10<ft>
    0:100<N>

Finally, all quantities in puny may be assigned an uncertainty. This error may be declared by placing the value in parentheses after the numerical value of a quantity, i.e.

    -234.7 (6.3) <cm>
    3e8 (1e7) <m s^-1>

These errors will propagate throughout any subsequent operations involving the quantity.
""")
        
    elif ARGS[0] == 'units':
        output("""
In puny, all quantities, vector or scalar, may have units associated with them. These units are declared as part of the numerical definition of the variable and are passed through any subsequent operations. The units are requested inside carats following the numerical value of a variable. For example if you wish to input a force of 2 Newtons as the variable named F, you would issue the command:

    F = 2<N>

The variable F now contains the value 2 along with the units Newtons^1. This variable is equivalent (insofar as double float precision conversion factors) to F= 200000<erg cm^-1>. After inputting a quantity you no longer need to worry about unit conversions. You must, however be sure that dimensional requirements are met. For instance you may not add a length to a mass or raise a number to anything but a dimension-less exponent.

Note that inside the carats no division signs may be used. The units must be simply raised to negative powers. So 9.8 m / s^2 should be defined by 9.8<m s^-2>

When certain operations are performed on your quantities, they may be converted into a combination of MKS (meter-kilogram-second standard) units. Therefore by default if you enter something like "32<ft>*5<AU>" you will receive the output "ans = 7.29558892416E+12 <m^2>". To force certain units on your result just use the "|in " post-command:

    >> 32<ft>*5<AU>|in pc^2
        ans = 7.66219065806E-21 <pc^2>

This post-command may also be used to perform quick conversions, like

    >> 3<days>|in nanoseconds
        ans = 2.592E+14 <ns>

or to find common constants in new units, like

    >> _c|in AU hr^-1
        _c = 7.21435972851 <AU hr^-1>
""")
        
    else:
        # not a hardcoded subject, maybe this is the name of a keyword or function
        
        try:
            # wanna catch io exceptions
            if ARGS[0] in KEYWORDS.keys():
                # this is a builtin keyword name
                helpfp = open(puny.punyDir + "keywords/"+ARGS[0]+".h", 'r')
                helptext = re.sub('\\\\\n','',helpfp.read().strip())    # use \ as line wrapper as in python
                output(ARGS[0]+' - '+KEYWORDS[ARGS[0]].description+"\nUsage: "+ARGS[0]+" "+KEYWORDS[ARGS[0]].usage+\
                    "\n--------------\n"+helptext)
                helpfp.close()
                
            elif ARGS[0] in EXTERNAL_KEYWORDS.keys():
                # this is an externally added keyword name
                helpfp = open(EXTERNAL_KEYWORD_DIR+ARGS[0]+".h", 'r')
                helptext = re.sub('\\\\\n','',helpfp.read().strip())    # use \ as line wrapper as in python
                output(ARGS[0]+' - '+EXTERNAL_KEYWORDS[ARGS[0]].description+"\nUsage: "+\
                    EXTERNAL_KEYWORDS[ARGS[0]].usage+"\n--------------\n"+helptext)
                helpfp.close()
                
            elif ARGS[0] in pmath.FUNCTIONS.keys():
                # this is a function name
                helpfp = open(puny.punyDir + "functions/"+pmath.FUNCTIONS[ARGS[0]].package+'/'+ARGS[0]+".h", 'r')
                helptext = re.sub('\\\\\n','',helpfp.read().strip())    # use \ as line wrapper as in python
                output('Package: '+pmath.FUNCTIONS[ARGS[0]].package+"\n"+ARGS[0]+' - '+pmath.FUNCTIONS[ARGS[0]].description+"\nUsage: "+\
                    pmath.FUNCTIONS[ARGS[0]].usage+"\n--------------\n"+helptext)
                helpfp.close()
            
            else:
                output("Unknown help subject: " + ARGS[0])
        
        except IOError:
            output("Could not open help file for subject: "+ARGS[0])
