# TODO: allow multiple arguments to rmws

REQWS = ARGS[0].upper()

if REQWS not in puny.WORKSPACES.keys():
    output('Workspace does not exist: '+REQWS)

else:
    for var in puny.WORKSPACES[REQWS].variables.keys():
        # delete variables
        del puny.WORKSPACES[REQWS].variables[var]
        
    for func in puny.WORKSPACES[REQWS].functions.keys():
        # delete variables
        del puny.WORKSPACES[REQWS].functions[func]
        
    for var in puny.WORKSPACES[REQWS].variables.keys():
        # delete variables
        del puny.WORKSPACES[REQWS].variables[var]
        
    del puny.WORKSPACES[REQWS]

    if len(puny.WORKSPACES) == 0:
        # need to have at least one workspace
        puny.WORKSPACES['DEFAULT'] = puny.workspace()

    if puny.DEFAULTWS == REQWS:
        # need to change workspaces
        puny.DEFAULTWS = puny.WORKSPACES.keys()[-1]
    
