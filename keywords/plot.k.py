# TODO: Use pylab/matplotlib
import Gnuplot, time

if len(ARGS[0]) != len(ARGS[1]):
    raise puny.exc("Cannot plot vector's whose lengths do not match")

points = []
for i in range(len(ARGS[0].values)):
    points.append([ARGS[0].values[i],ARGS[1].values[i]])

plot = Gnuplot.Gnuplot(debug=0)
plot('set data style points')
# set default title
plot.title(ARGS[1].name+' vs. '+ARGS[0].name)
plot.plot(points)
time.sleep(.5)  # we have to sleep because of messed up fifos
