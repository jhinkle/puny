#!/usr/bin/python
"""

    qpuny - PyQt frontend for Puny
    Copyright (C) 2003  Jacob Hinkle

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""
import re, sys, copy, traceback, string, os, ConfigParser

from qt import *

import puny, pkwstd

class cmdlineEdit(QLineEdit):
    def __init__(self, *args):
        apply(QLineEdit.__init__, (self, ) + args)

        self.cmdhistory = []
        self.cmdmatches = None
        self.matchpos = 0

        font = QFont()
        font.setFamily("Courier New")
        self.setFont(font)

    def keyPressEvent(self, ev):
        if ev.key() == Qt.Key_Return or ev.key() == Qt.Key_Enter:
            cmdline = self.text().ascii()

            if not re.match('\s*$', cmdline):
                self.cmdhistory.append(cmdline)

                QLineEdit.keyPressEvent(self, ev)
                self.clear()

        elif ev.key() == Qt.Key_Up:
            if self.cmdmatches == None:
                # get a list of matches
                self.cmdmatches = [self.text().ascii()]
                for i in range(len(self.cmdhistory)-1, -1, -1):
                    if re.match(self.text().ascii(), self.cmdhistory[i]):
                        self.cmdmatches.append(self.cmdhistory[i])

            if self.matchpos < len(self.cmdmatches) - 1:
                self.matchpos += 1
                self.setText(self.cmdmatches[self.matchpos])

        elif ev.key() == Qt.Key_Down:
            if self.cmdmatches != None and self.matchpos > 0:
                self.matchpos -= 1
                self.setText(self.cmdmatches[self.matchpos])

        elif ev.key() == Qt.Key_Tab:
            pass

        # TODO: recognize operator keys (+,-,*,/,^) and insert previously set variable before text if string empty)
        else:
            # let other keys be handled normally
            if self.cmdmatches != None:
                del self.cmdmatches
                self.cmdmatches = None
                self.matchpos = 0
            QLineEdit.keyPressEvent(self,ev)
            return

        ev.accept()

class QPuny(QMainWindow):
    def __init__(self, *args):
        apply(QMainWindow.__init__, (self, ) + args)
        self.setCaption("Calculation program")

        # check for exiting qpunyrc and if exists load it
        # otherwise just load the default one (default.conf resides in punyDir)
        self.loadConfig()

        # load window to correct size
        self.move(self.config.getint('Geometry','XPos'),self.config.getint('Geometry','YPos'))
        self.resize(self.config.getint('Geometry','Width'),self.config.getint('Geometry','Height'))
        self.clearWState(Qt.WState_Polished)
        
        # TODO: reload stored dock window size/position/show or hide/dock position
        # Set up workspace viewer dockable window
        self.wsDock = QDockWindow()
        self.wsDock.setResizeEnabled(1)
        self.wsDock.setCloseMode(QDockWindow.Always)
        self.addDockWindow(self.wsDock, Qt.DockLeft)
        self.wsView = QListView(self.wsDock,"wsView")
        wsfont = QFont()
        wsfont.setFamily("Courier New")
        self.wsView.setFont(wsfont)
        self.wsDock.setWidget(self.wsView)
        self.wsDock.setCaption(self.__tr("Workspace Viewer"))
        self.wsDock.show()
        self.setAppropriate(self.wsDock, 1)

        # Set the + sign to appear
        self.wsView.setRootIsDecorated(True)

        # Add column headers
        self.wsView.addColumn(self.__tr("Name"))
        self.wsView.addColumn(self.__tr("Value"))
        self.wsView.addColumn(self.__tr("Units/Dimensions"))

        # Main area (contained in QVBoxLayout)
        LayoutWidget = QWidget(self,"layout2")
        mainLayout = QVBoxLayout(LayoutWidget,0,5,"layout2")

        # Output Window
        self.mainView = QTextEdit(LayoutWidget,"mainView")
        mainView_font = QFont(self.mainView.font())
        mainView_font.setFamily("Courier New")
        self.mainView.setFont(mainView_font)
        self.mainView.setTextFormat(QTextEdit.PlainText)
        self.mainView.setLinkUnderline(0)
        self.mainView.setOverwriteMode(0)
        self.mainView.setReadOnly(1)
        self.mainView.setTabChangesFocus(1)
        mainLayout.addWidget(self.mainView)

        # Input Widget
        self.inputEdit = cmdlineEdit(LayoutWidget,"inputEdit")
        mainLayout.addWidget(self.inputEdit)

        # set main widget to the main area layout
        self.setCentralWidget(LayoutWidget)

        # get the translations done for the main window
        self.languageChange()

        # Menu
        mb = self.menuBar()

        self.fileMenu = QPopupMenu()
        mb.insertItem(self.__tr("&File"), self.fileMenu)

        self.windowMenu = QPopupMenu(self)
        mb.insertItem(self.__tr('&Window'), self.windowMenu)
        self.connect(self.windowMenu, SIGNAL('aboutToShow()'), self.handleShowWindowMenu)

        self.helpMenu = QPopupMenu()
        mb.insertItem(self.__tr("&Help"), self.helpMenu)

        # Status Bar
        self.status = QStatusBar(self)

        # Set up Actions
        # Workspace Viewer toggle action
        self.wsAction = QAction(self.__tr('Workspace Viewer'),
                self.__tr('&Workspace Viewer'),0,self,None,1)
        self.wsAction.setStatusTip(self.__tr('Toggle the Workspace Viewer window'))
        self.wsAction.setWhatsThis(self.__tr(
            """<b>Toggle the Workspace Viewer window</b>"""
            """<p>If the Workspace Viewer window is hidden then display it. It is displayed"""
            """ then close it.</p>"""))
        self.connect(self.wsAction, SIGNAL('activated()'), self.handleWSViewer)

        # Define  Quit action
        self.fileQuitAction = QAction(self.tr("Quit the program"), \
                      #QIconSet(QPixmap([])), \
                      self.tr("&Quit"), \
                      QAccel.stringToKey(self.tr("CTRL+Q", \
                                                 "File|Quit")), \
                      self)
        self.fileQuitAction.addTo(self.fileMenu)

        # CONNECTIONS
        self.connect(self.fileQuitAction, SIGNAL("activated()"), self.handleQuit)
        self.connect(qApp, SIGNAL("aboutToQuit()"), self.handleQuit)
        
        self.connect(self.inputEdit, SIGNAL("returnPressed()"), self.execCommand)

        #self.connect(self.mainView, SIGNAL())

        # OTHER
        self.cmdhistory = []

        self.updateWSView()

        # put our keywords in here
        pkwstd.output = self.output     # so that keywords can output stuff
        pkwstd.EXTERNAL_KEYWORD_DIR = puny.punyDir + 'qpuny_keywords/'  # to look up help files mainly

        # let puny know about our extra keywords
        pkwstd.EXTERNAL_KEYWORDS['exit'] = pkwstd.keyword(name='exit', description='Exit the program', usage='exit', minargs=0)
        pkwstd.EXTERNAL_KEYWORDS['quit'] = pkwstd.keyword(name='quit', description='Exit the program', usage='quit', minargs=0)
        pkwstd.EXTERNAL_KEYWORDS['clc'] = pkwstd.keyword(name='clc', description='Clear the output window', usage='exit', minargs=0)

    def polish(self):
        """
        Do things that require all widgets to be initialized first
        """
        if not self.testWState(Qt.WState_Polished) and self.mainView.text() == '':
            QMainWindow.polish(self)

            self.output("Puny v"+puny.punyVersion+"\nType 'help' for assistance.")

    # TODO: Move these two functions to puny.py so we can take advantage of them all over
    def loadConfig(self):
        """
        Loads simple configuration from ~/.puny/qpuny.conf
        """        
        self.config = ConfigParser.SafeConfigParser()
        self.config.read([puny.punyDir+'/qpuny-default.conf',os.path.expanduser('~/.puny/qpuny.conf')])
    
    def storeConfig(self):
        """
        Stores configuration in ~/.puny/qpuny.conf
        """
        try:
            # add the needed sections
            self.config.add_section('Geometry')
        except ConfigParser.DuplicateSectionError:
            # don't worry if we already have the sections
            pass
            
        self.config.set('Geometry','XPos',str(self.x()))
        self.config.set('Geometry','YPos',str(self.y()))
        self.config.set('Geometry','Width',str(self.width()))
        self.config.set('Geometry','Height',str(self.height()))
        
        # TODO: store dock window geometry here
        
        if not os.path.isdir(os.path.expanduser('~/.puny/')):
            os.makedirs(os.path.expanduser('~/.puny/'))

        configFile = open(os.path.expanduser('~/.puny/qpuny.conf'),'w')
        self.config.write(configFile)
        configFile.close()
        

    def keyword_callback(self, kw, args):
        """
        A special keyword handler which will add some keywords which are special to this gui
        """
        if kw == 'exit' or kw == 'quit':
            self.handleQuit()
            return True # not that we ever get here...

        elif kw == 'clc':
            self.mainView.clear()
            return True

        else:
            # now see if it's a builtin
            return pkwstd.exec_keyword(kw, args)

    def languageChange(self):
        self.setCaption(self.__tr("QPuny"))
        self.wsView.header().setLabel(0,self.__tr("Name"))
        self.wsView.header().setLabel(1,self.__tr("Value"))
        self.wsView.header().setLabel(2,self.__tr("Units/Dimensions"))

    def output(self, string):
        """
        Output a string to the main window (with trailing newline)
        """
        self.mainView.setText(self.mainView.text().append(string+"\n")) # segmentation fault
        self.mainView.moveCursor(QTextEdit.MoveEnd, 0)

    def execCommand(self):
        cmdline = self.inputEdit.text().ascii()

        self.output("\n>> " + cmdline)

        try:
            puny.execute(string = cmdline, outputcallback = self.output, kwcallback=self.keyword_callback)
        except puny.exc, err:
            self.status.message('An error occurred!', 1500)
            self.output("ERROR: " + str(err))
            return

        self.updateWSView()


    def updateWSView(self):
        """
        Update the workspace viewer
        """
        self.wsView.clear()
        self.wsViewItems = {}

        wsnames = puny.WORKSPACES.keys()
        wsnames.sort()

        # TODO: split members by type, omitting empty types (i.e. no functions defined)
        for WS in wsnames:
            # first set up a listitem for this workspace
            self.wsViewItems[WS] = {}
            if WS == puny.DEFAULTWS:
                self.wsViewItems[WS][0] = QListViewItem(self.wsView, "*** "+WS+" ***")
            else:
                self.wsViewItems[WS][0] = QListViewItem(self.wsView, WS)
            self.wsViewItems[WS][0].setOpen(True)

            varnames = copy.copy(puny.WORKSPACES[WS].variables.keys())
            varnames.sort()
            for varname in varnames:
                var = puny.WORKSPACES[WS].variables[varname]

                if len(var.values) > 1:
                    valstr = str(len(var.values))+'-VECTOR'

                else:
                    valstr = var.valueString(var.values[0], var.error[0])

                unitstrs = []
                for unit in var.units.keys():
                    if var.units[unit] == 1:
                        unitstrs.append(puny.UNITS[unit].abbrev)
                    else:
                        unitstrs.append(puny.UNITS[unit].abbrev + '^' + str(var.units[unit]))
                unitstr = ' '.join(unitstrs)

                self.wsViewItems[WS][varname] = QListViewItem(self.wsViewItems[WS][0], varname, valstr, unitstr)
            
            symnames = copy.copy(puny.WORKSPACES[WS].symbols.keys())
            symnames.sort()
            for symname in symnames:
                sym = puny.WORKSPACES[WS].symbols[symname]
                
                self.wsViewItems[WS][symname] = QListViewItem(self.wsViewItems[WS][0], symname, 'symbol', sym.dimString())

    def toggleWindow(self, w):
        """
        If window is hidden then show it. If shown then hide it.

        @param w reference to the window to be toggled
        """
        if w.isHidden():
            w.show()
        else:
            w.hide()

    def handleQuit(self):
        # store configuration
        self.storeConfig()
        
        qApp.closeAllWindows()

    def handleWSViewer(self):
        """
        Private slot to toggle the workspace viewer
        """
        self.toggleWindow(self.wsDock)

    def handleShowWindowMenu(self):
        """
        Private slot to display the Window menu.
        """
        self.windowMenu.clear()

        #self.windowMenu.insertTearOffHandle()

        # Set the options according to what is being displayed.
        self.wsAction.addTo(self.windowMenu)

        self.wsAction.setOn(not self.wsDock.isHidden())

    def exceptionHook(self, exc_type, exc_value, exc_traceback):
        """
        New exception handler, to be installed at sys.excepthook
        """
        msg = 'ERROR: '+str(exc_value)+'\n\n' + \
                           string.joinfields(traceback.format_exception(exc_type,
                           exc_value,
                           exc_traceback))

        # show our popup
        QMessageBox.critical(self, "ERROR", msg)

        # call default exception hook to display on terminal
        sys.__excepthook__(exc_type, exc_value, exc_traceback)

    def __tr(self,s,c = None):
        return qApp.translate("QPuny",s,c)

def run_qpuny():
    """
    Run the Qt interface.
    """
    app = QApplication(sys.argv)
    f = QPuny()
    sys.excepthook = f.exceptionHook
    f.inputEdit.setFocus() # set focus to input box
    f.show()
    app.setMainWidget(f)
    # FIXME: Segmentation fault on exit every time
    app.exec_loop()

if __name__ == '__main__':
    # this file is being run directly
    run_qpuny()
