###########################################################################
#    Copyright (C) 2005 by Jacob Hinkle
#    <hinklejd@muohio.edu>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################

import puny
import sys, glob

from distutils.core import setup

setup(name='puny',
    version=puny.punyVersion,
    description='Physical quantity calculation utility',
    author='Jacob Hinkle',
    author_email='hinklejd@muohio.edu',
    url='http://puny.sourceforge.net/',
    py_modules=['puny','pmath','pkwstd','qpuny'],
    scripts=['qpuny','puny_compile_builtins.py'],
    data_files=[('puny/functions',glob.glob('functions/*')),
                ('puny/keywords',glob.glob('keywords/*')),
                ('puny/qpuny_keywords',glob.glob('qpuny_keywords/*'))])