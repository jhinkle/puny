"""

    const.py - constant definitions
    Copyright (C) 2003  Jacob Hinkle

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

TYPE_ARITH = 0
TYPE_FUNC = 1
TYPE_SYM = 2
TYPE_VAR = 3
TYPE_CONST = 4

OP_ADD = 0
OP_SUB = 1
OP_MUL = 2
OP_DIV = 3
OP_EXP = 4

OP_CHAR = {}
OP_CHAR[OP_ADD] = '+'
OP_CHAR[OP_SUB] = '-'
OP_CHAR[OP_MUL] = '*'
OP_CHAR[OP_DIV] = '/'
OP_CHAR[OP_EXP] = '^'
