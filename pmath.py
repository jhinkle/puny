
"""

    pmath - Puny mathematical functions
    Copyright (C) 2003  Jacob Hinkle

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

import code, math, types, copy, cPickle, marshal, re

import puny

class function:
    def __init__(self, name, minargs, maxargs, stringargs, usage, description, package):
        """
        Construct a function object with minimum and maximum # of args

        funcstring should be valid Python code which should set OUT as a
        quantity object. Arguments are put in a list called ARGS

        @param name name of function
        @param minargs minimum number of arguments
        @param maxargs maximum number of arguments (-1 for no limit)
        @param usage usage information string
        @param description short description string
        @param pacage package which provides this function
        """
        self.name = name
        self.minargs = minargs
        self.maxargs = maxargs
        self.stringargs = copy.copy(stringargs)
        self.usage = usage
        self.description = description
        self.package = package
        self.func = None

    def __call__(self, *rawargs):
        """
        Call a function, giving expected arguments directly
        """
        
        # TODO: don't define this here once we start changing function files
        OUT = puny.quantity()
        QuantityType = type(OUT)

        if len(rawargs) < self.minargs:
            # not enough arguments
            raise puny.exc('Minimum # of arguments not supplied')
        if self.maxargs >= 0 and len(rawargs) > self.maxargs:
            # not enough arguments
            raise puny.exc('Too many arguments supplied')

        ARGS = []
        for i in range(len(rawargs)):
            if type(rawargs[i]) == types.IntType or type(rawargs[i]) == types.FloatType:
                # raw number given
                ARGS.append(puny.quantity(values=[rawargs[i]]))

            elif type(rawargs[i]) == types.StringType:
                # convert to string if not a string argument
                if i in self.stringargs:
                    ARGS.append(rawargs[i])
                else:
                    ARGS.append(puny.evaluate(string=rawargs[i]))

            elif type(rawargs[i]) == types.ListType:
                # list given
                ARGS.append(puny.quantity(values=rawargs[i]))

            elif isinstance(rawargs[i], puny.quantity):
                # quantity, so just append this
                ARGS.append(rawargs[i])

            else:
                # not a quantity
                raise puny.exc('Argument is of invalid type')

        if self.func == None:
            # function not loaded yet

            try:
                compfp = open(puny.punyDir + "functions/" + self.package+'/'+self.name + ".fc", 'rb')

                # load compiled code from file
                self.func = marshal.load(compfp)

                compfp.close()
            except IOError:
                # else load and compile code from text file ... + ".f.py"
                try:
                    textfp = open(puny.punyDir + "functions/" + self.package+'/'+self.name + ".f.py", 'r')
                    code = textfp.read()
                    textfp.close()
                except IOError:
                    raise puny.exc('Could not open function file: '+self.package+'/'+self.name+'.f.py (or corresponding *.fc file)')

                self.func = compile(code, puny.punyDir + "functions/" + self.package+'/'+self.name + ".f.py", 'exec')

        # we have compiled code so execute it
        exec(self.func)

        if not 'OUT' in vars().keys():
            raise exc('Serious problem: function '+self.name+' does not declare OUT variable!')
        else:            
            return copy.deepcopy(OUT)

# Load function information
fnindexfp = open(puny.punyDir+'functions/index.pickle', 'rb')
FUNCTIONS = cPickle.load(fnindexfp)
fnindexfp.close()

# TODO: all functions must handle function and symbol arguments (automatic for arithmetic)

##logbase = function(minargs=2, maxargs=2, stringargs=[], usage="logbase(<base>, <x>)", \
##description="Arbitrary-base logarithm", \
##help="""
##Takes the logarithm of x with the base specified by base. Both base and x must be unit-less and dimension-less.
##""", \
##funcstring="""
##if (sizeof($QARGS[0]->units) != 0 OR sizeof($QARGS[1]->units) != 0)
##{
##    output("ERROR(logbase): Logarithm function requires unit-less parameters!");
##    $RESULT = FAILURE;
##}
##else if (sizeof($QARGS[0]->value) != 1)
##{
##    output("ERROR(logbase): Logarithm function requires a single base.");
##    $RESULT = FAILURE;
##}
##else
##{
##    // output unit-less, PURE number
##    $LOGB = log($QARGS[0]->value[0]);
##
##    for ($i = 0;$i < sizeof($QARGS[1]->value);$i++)
##    {
##        $OUT->value[$i] = log($QARGS[1]->value[$i])/$LOGB;
##            // D(log(B, A)) = dA / (A * ln(B))
##        $OUT->error[$i] = $QARGS[1]->error[$i]/($QARGS[1]->value[$i]*$LOGB);
##    }
##
##    $OUT->units = array();
##    $OUT->dimensions = array();
##}
##""")
##FUNCTIONS['logbase'] = logbase
##
##count = function(minargs=1, maxargs=1, stringargs=[], usage="count(<vector>)", \
##description="Count number of elements in vector", \
##help="""
##Returns the number of elements in a vector as a scalar integer. Note that it does not sum the values, merely counts how many elements are present.
##""", \
##funcstring="""
##$OUT->value[0] = sizeof($QARGS[0]->value);
##$OUT->error[0] = 0; // exact integer
##""")
##FUNCTIONS['count'] = count
##
##cross = function(minargs=2, maxargs=2, stringargs=[], usage="cross(<a>,<b>)", \
##description="Cross-product", \
##help="""
##Takes the cross product of two 3-dimensional vectors.
##""", \
##funcstring="""
##$A = $QARGS[0];
##$B = $QARGS[1];
##
##$TMP = new Quantity();
##
##$TMP->value[0] = $A->value[1]*$B->value[2] - $A->value[2]*$B->value[1];
##    // d(A*B - C*D) = sqrt((B*dA)^2 + (A*dB)^2 + (D*dC)^2 + (C*dD)^2)
##$TMP->error[0] = sqrt(pow($A->error[1]*$B->value[2], 2)
##                    + pow($A->value[1]*$B->error[2], 2)
##                    + pow($A->error[2]*$B->value[1], 2)
##                    + pow($A->value[2]*$B->error[1], 2));
##
##$TMP->value[1] = $A->value[2]*$B->value[0] - $A->value[0]*$B->value[2];
##$TMP->error[1] = sqrt(pow($A->error[2]*$B->value[0], 2)
##                    + pow($A->value[2]*$B->error[0], 2)
##                    + pow($A->error[0]*$B->value[2], 2)
##                    + pow($A->value[0]*$B->error[2], 2));
##
##$TMP->value[2] = $A->value[0]*$B->value[1] - $A->value[1]*$B->value[0];
##$TMP->error[2] = sqrt(pow($A->error[0]*$B->value[1], 2)
##                    + pow($A->value[0]*$B->error[1], 2)
##                    + pow($A->error[1]*$B->value[0], 2)
##                    + pow($A->value[1]*$B->error[0], 2));
##$U = $B;
##$U->value = array(1); // U is a unit scalar with units of B
##$U->error = array(0);
##
##$TMP->units = $A->units;
##$TMP->dimensions = $A->dimensions;
##
##operation_exec(OPMUL, $TMP, $U, $OUT);
##""")
##FUNCTIONS['cross'] = cross
##
##dot = function(minargs=2, maxargs=2, stringargs=[], usage="dot(<a>,<b>)", \
##description="Dot Product", \
##help="""
##Calculates the dot product (a scalar) of two arbitrarily long vectors.
##""", \
##funcstring="""
##if (sizeof($QARGS[0]->value) != sizeof($QARGS[1]->value))
##{
##    output('ERROR(dot): Vectors must have equal lengths for dot product');
##
##    $RESULT = FAILURE;
##}
##else
##{
##    $OUT->value[0] = 0;
##
##    $TMP_ERROR = 0;
##
##    /* ERROR PROPAGATION : dot(A, B)
##     * A = (a1, a2, ... , an)
##     * B = (b1, b2, ... , bn)
##     * D(dot(A, B)) = sqrt((a1*db1)^2 + (b1*da1)^2 + ... + (an*dbn)^2 + (bn/da1)^2)
##     */
##
##    for ($i = 0;$i < sizeof($QARGS[0]->value);$i++)
##    {
##        $OUT->value[0] += $QARGS[0]->value[$i] * $QARGS[1]->value[$i];
##        $TMP_ERROR += pow($QARGS[0]->value[$i]*$QARGS[1]->error[$i], 2) +
##                      pow($QARGS[1]->value[$i]*$QARGS[0]->error[$i], 2);
##    }
##
##    $OUT->error[0] = sqrt($TMP_ERROR); // propagate like a son-bitch...
##
##    $U = $QARGS[1];
##    $U->value = array(1); // U is a precise 1 scalar with units of B
##    $U->error = array(0); //    U is perfectly precise scalar 1
##
##    $OUT->units = $QARGS[0]->units;
##    $OUT->dimensions = $QARGS[0]->dimensions;
##
##    operation_exec(OPMUL, $OUT, $U, $OUT);
##}
##""")
##FUNCTIONS['dot'] = dot
##
##sum = function(minargs=1, maxargs=1, stringargs=[], usage="sum(<v>)", \
##description="Sum the elements of a vector", \
##help="""
##Returns the sum of all elements in the vector x.
##""", \
##funcstring="""
##$OUT->units = $QARGS[0]->units;
##$OUT->dimensions = $QARGS[0]->dimensions;
##
##$OUT->value[0] = 0;
##$TMP_ERROR = 0;
##
##for ($i=0;$i < sizeof($QARGS[0]->value);$i++)
##{
##    $OUT->value[0] += $QARGS[0]->value[$i];
##    $TMP_ERROR += pow($QARGS[0]->error[$i], 2);
##}
##
##$OUT->value[0] = array_sum($QARGS[0]->value);
##$OUT->error[0] = sqrt($TMP_ERROR);
##""")
##FUNCTIONS['sum'] = sum
##
##mag = function(minargs=1, maxargs=1, usage="mag(<v>)", \
##description="Magnitude of a vector", \
##help="""
##Calculates the magnitude of a given vector.
##""", \
##funcstring="""
##$OUT->units = $QARGS[0]->units;
##$OUT->dimensions = $QARGS[0]->dimensions;
##
##$TMP = 0;
##$TMP_ERROR = 0;
##
##for ($i=0;$i < sizeof($QARGS[0]->value);$i++)
##{
##    $TMP += pow($QARGS[0]->value[$i], 2);
##    $TMP_ERROR += pow($QARGS[0]->value[$i] * $QARGS[0]->error[$i], 2);
##}
##
##$OUT->value[0] = sqrt($TMP);
##$OUT->error[0] = sqrt($TMP_ERROR) / $OUT->value[0];
##""")
##FUNCTIONS['mag'] = mag
##
##acos = function(minargs=1, maxargs=1, stringargs=[], usage="acos(<x>)", \
##description="Arc cosine function", \
##help="""
##Returns the arc cosine of x in radians. x must be unit-less and dimension-less and -1 <= x <= +1.
##""", \
##funcstring="""
##if (sizeof($QARGS[0]->units) != 0)
##{   // failure case
##    output("ERROR(acos): arc cosine function requires unit-less parameter!");
##    $RESULT = FAILURE;
##}
##else
##{
##    foreach ($QARGS[0]->value AS $VAL)
##    {
##        if ($VAL < -1 OR $VAL > 1)
##        {
##            output('ERROR(acos): Input values must be between -1 and +1.');
##        }
##    }
##
##    // output is unit-less, PURE number
##    for ($i = 0;$i < sizeof($QARGS[0]->value);$i++)
##    {
##        $OUT->value[$i] = acos($QARGS[0]->value[$i]);
##            // D(acos(A)) = dA / sqrt(1 - A^2)
##        $OUT->error[$i] = ($QARGS[0]->value[$i] == 1 ) ? 0 : abs($QARGS[0]->error[$i])/sqrt(1 - pow($QARGS[0]->value[$i], 2));
##    }
##
##    $OUT->units = array();
##    $OUT->dimensions = array();
##}
##""")
##FUNCTIONS['acos'] = acos
##
##asin = function(minargs=1, maxargs=1, stringargs=[], usage="asin(<x>)", \
##description="Arc sine function", \
##help="""
##Returns the arc sine of x in radians. x must be unit-less and dimension-less and -1 <= x <= +1.
##""", \
##funcstring="""
##if (sizeof($QARGS[0]->units) != 0)
##{   // failure case
##    output("ERROR(asin): arc sine function requires unit-less parameter!");
##    $RESULT = FAILURE;
##}
##else
##{
##    foreach ($QARGS[0]->value AS $VAL)
##    {
##        if ($VAL < -1 OR $VAL > 1)
##        {
##            output('ERROR(asin): Input values must be between -1 and +1.');
##        }
##    }
##
##    // output is unit-less, PURE number
##    for ($i = 0;$i < sizeof($QARGS[0]->value);$i++)
##    {
##        $OUT->value[$i] = asin($QARGS[0]->value[$i]);
##            // D(acos(A)) = dA / sqrt(1 - A^2)
##        $OUT->error[$i] = ($QARGS[0]->value[$i] == 1) ? 0 : abs($QARGS[0]->error[$i])/sqrt(1 - pow($QARGS[0]->value[$i], 2));
##    }
##
##    $OUT->units = array();
##    $OUT->dimensions = array();
##}
##""")
##FUNCTIONS['asin'] = asin
##
##atan = function(minargs=1, maxargs=1, stringargs=[], usage="atan(<x>)", \
##description="Arc tangent function", \
##help="""
##Returns the arc cosine of x in radians. x must be unit-less and dimension-less.
##""", \
##funcstring="""
##if (sizeof($QARGS[0]->units) != 0)
##{   // failure case
##    output("ERROR(atan): arc tangent function requires unit-less parameter!");
##    $RESULT = FAILURE;
##}
##else
##{
##    // output is unit-less, PURE number
##    for ($i = 0;$i < sizeof($QARGS[0]->value);$i++)
##    {
##        $OUT->value[$i] = atan($QARGS[0]->value[$i]);
##            // D(acos(A)) = dA / sqrt(1 + A^2)
##        $OUT->error[$i] = abs($QARGS[0]->error[$i])/sqrt(1 + pow($QARGS[0]->value[$i], 2));
##    }
##
##    $OUT->units = array();
##    $OUT->dimensions = array();
##}
##""")
##FUNCTIONS['atan'] = atan
##
##atan2 = function(minargs=2, maxargs=2, stringargs=[], usage="atan2(<y>, <x>)", \
##description="Arc tangent function of 2 variables", \
##help="""
##Returns the arc tangent of x and y. It is the same basically as atan(<y>/<x>) except that the signs of both variables are used to determine in which quadrant the resultant angle should lie. Both x and y should be unit-less and dimension-less and should be the same size.
##""", \
##funcstring="""
##if (sizeof($QARGS[0]->value) != sizeof($QARGS[1]->value))
##{
##    output('ERROR(atan2): both arguments must be the same size!');
##}
##else if (sizeof($QARGS[0]->units) != 0 OR sizeof($QARGS[1]->units) != 0)
##{   // failure case
##    output('ERROR(atan2): arc tangent function requires unit-less parameters!');
##    $RESULT = FAILURE;
##}
##else
##{
##    // output is unit-less, PURE number
##    for ($i = 0;$i < sizeof($QARGS[0]->value);$i++)
##    {
##        if ($QARGS[1]->value[$i] == 0)
##        {  // zero
##            $OUT->value[$i] = ($QARGS[0]->value[$i] > 0) ? pi()/2 : -pi()/2;
##            $OUT->error[$i] = 0;
##        }
##        else
##        {
##            $OUT->value[$i] = atan2($QARGS[0]->value[$i], $QARGS[1]->value[$i]);
##                // D(A/B) = abs(A/B)*sqrt((dB/B)^2 + (dA/A)^2)
##            $DIV_ERR = abs($OUT->value[$i]) * sqrt(pow($QARGS[1]->error[$i] / $QARGS[1]->value[$i], 2) + pow($QARGS[0]->error[$i] / $QARGS[0]->value[$i], 2));
##                // D(atan(A)) = dA / sqrt(1 + A^2)
##            $OUT->error[$i] = abs($DIV_ERR)/sqrt(1 + pow($OUT->value[$i], 2));
##        }
##    }
##
##    $OUT->units = array();
##    $OUT->dimensions = array();
##}
##""")
##FUNCTIONS['atan2'] = atan2
##
