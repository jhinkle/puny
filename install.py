#!/usr/bin/env python
"""

    installation script for puny project
    Copyright (C) 2003  Jacob Hinkle

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

import sys

# check arguments for --help
if '--help' in sys.argv:
    print """
puny install script. Options:

--prefix=[dir]      install puny into the following directory (default=$PY
"""
    sys.exit()

# check requirements
    # has PyQt
    # has write permission to

# compile builtin functions and keywords

# precompile all python files

# create target directory in site-packages

# copy files to target directory

# copy qpuny to some place in path (i.e. /usr/bin/)
