
"""

    puny - physical quantity module
    Copyright (C) 2003  Jacob Hinkle

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

import re, math, copy, types, os, sys, cPickle

punyVersion = '0.1'

MAXVECTORSIZE = 50000  # an arbitrary upper limit to vector size

# stole this stuff from the eric3 code
try:
    punyDir = os.path.dirname(__file__)
except:
    punyDir = os.path.dirname(sys.argv[0])
if punyDir == '':
    punyDir = os.getcwd()
punyDir = os.path.abspath(punyDir)+"/"

### Load companion modules here, so that punyDir is set already
import pmath, const

class exc(Exception):
    """
    Generic exception
    """
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
        
class UnitConflictError(exc):
    """
    Units conflict for a given operation (i.e. addition or subtraction)
    """

class ArgumentError(exc):
    """
    Number of arguments to a given function is incorrect
    """
    
class ComparisonError(exc):
    """
    Two objects were compared for whom it makes no sense (symbols, functions)
    """

class unit:
    "A unit object including information about conversion and dimension"
    def __init__(self, name, abbrev, aliases, dimensions, base_unit, base_dimension, \
            to_base, to_base_offset, from_base, from_base_offset):
        """
        Construct a unit object
        """
        self.name = name
        self.abbrev = abbrev
        self.aliases = aliases
        self.dimensions = dimensions
        self.base_unit = base_unit
        self.base_dimension = base_dimension
        self.to_base = to_base
        self.to_base_offset = to_base_offset
        self.from_base = from_base
        self.from_base_offset = from_base_offset
        
    def dimString(self, long_format=0):
        """
        Translate a dimension dict into a string.
        
        The long_format option will cause the dimensions to be spelled out as opposed to the abbreviations.
        """
        return dict_to_dimstring(self.dimensions, long_format)

class workspace:
    def __init__(self):
        """
        Initialize a workspace object
        """
        self.variables = {}
        self.functions = {}
        self.symbols = {}

    def conflict(self, name):
        """
        Check to see if this name conflicts with any already in the workspace
        """
        return name in self.variables.keys() or \
            name in self.functions.keys() or \
            name in self.symbols.keys()

    def setMember(self, entity):
        """
        Insert or update a member of the workspace
        
        Note that this function will delete existing members of the same name
        """
        for entity_dict in [self.variables, self.functions, self.symbols]:
            if entity.name in entity_dict.keys():
                # this name exists in workspace, clear it
                del entity_dict[entity.name]
        
        if entity.__class__ == quantity:
            self.variables[entity.name] = entity
        elif entity.__class__ == symbol:
            self.symbols[entity.name] = entity
        elif entity.__class__ == function:
            self.functions[entity.name] = entity
        else:
            raise exc('Argument to workspace.setMember is of unrecognized type: '+str(entity.__class__))
def findbase(DIM):
    """
    Find a base unit for a given dimension
    """
    global UNITS

    for i in UNITS:
        if UNITS[i].base_dimension == DIM:
            return i

def findunit(UNIT):
    """
    Find a named unit, return index or None on failure
    """
    global UNITS

    for i in UNITS.keys():
        if UNITS[i].name == UNIT or \
                UNITS[i].abbrev == UNIT or \
                UNIT in UNITS[i].aliases:
            return i

    return None

def getunitdims(UNIT):
    """
    Find dimensions of unit.

    Returns a dictionary of dimensions or None if unit not found
    """
    global UNITS

    if UNIT not in UNITS.keys():
        raise exc('Unit ' + str(UNIT) + ' not found in UNITS!')

    dims = dimension_dict()
    if UNITS[UNIT].base_dimension != 0:
        # base unit
        dims[UNITS[UNIT].base_dimension] = 1
    else:
        dims = copy.deepcopy(UNITS[UNIT].dimensions)

    return dims

class dimension_dict(dict):
    """
    Dimension dict. This is just a regular dict with the added ability to multiply, divide, and exponentiate
    """
    def __mul__(self, B):
        """
        Multiply two dimension arrays
        """
        if B.__class__ != dimension_dict:
            raise exc('Illegal type for multiplication by dimension_dict')
        
        # return a copy of self with dimensions adjusted by those of B
        OUT = copy.deepcopy(self)
        
        for DIM in B.keys():
                if OUT.has_key(DIM):
                    OUT[DIM] += B[DIM]
                    
                    # Now remove it if exponent is zero
                    if OUT[DIM] == 0:
                        del OUT[DIM]
                else:
                    OUT[DIM] = B[DIM]
        return OUT
        
    def __div__(self, B):
        """
        Divide two dimension arrays
        """
        if B.__class__ != dimension_dict:
            raise exc('Illegal type for dividing dimension_dict')
        
        # return a copy of self with dimensions adjusted by those of B
        OUT = copy.deepcopy(self)
        
        for DIM in B.keys():
                if OUT.has_key(DIM):
                    OUT[DIM] -= B[DIM]
                    
                    # Now remove it if exponent is zero
                    if OUT[DIM] == 0:
                        del OUT[DIM]
                else:
                    OUT[DIM] = -B[DIM]
        
        return OUT

    def __pow__(self, B):
        """
        Exponentiate dimension array by power
        """
        if B.__class__ not in [float, int]:
            raise exc('Illegal type for exponentiating dimension_dict')
        
        # For exponentiation we'll create the dict as we go
        OUT = dimension_dict()
        
        for DIM in self.keys():
            OUT[DIM] = self[DIM] * B
        
        return OUT

class math_entity:
    """
    Base class for quantities, symbols, and functions
    
    All objects of this type carry dimensions and names, along with the machinery to format those objects.
    
    If extra dicts or lists are employed then the copy function must be overwritten
    """
    def __init__(self, name='', dimensions=dimension_dict()):
        """
        Initialize entity
        """
        self.name = name
        if name != '':
            # provided a name
            self.setName(name)         
        self.dimensions = dimensions
    
    def __copy__(self):
        """
        Return a copy of entity.

        Note, here we make copies of any lists, tuples, or dictionaries then assign them
        to our resultant quantity so that we don't double-reference any containers
        """
        # need to copy some stuff here
        dimcpy = copy.deepcopy(self.dimensions)

        return math_entity(name=self.name, dimensions=dimcpy)

    def setName(self, string):
        """
        Safely try and name this entity

        Does not return value, but raises exception on illegal name
        """
        if string == '' or \
                re.compile('^[0-9_]').match(string) != None or \
                re.compile('[^0-9a-zA-Z_]').search(string) != None:
            raise exc("Illegal variable name: '" + string + "'")

        self.name = string
        
    def formalName(self):
        """
        Return the formal reference for this object.
        
        This function is meant to be overridden, i.e. puny.function
        """
        return self.name

    def dimString(self, long_format=0):
        """
        Translate a dimension dict into a string.
        
        The long_format option will cause the dimensions to be spelled out as opposed to the abbreviations.
        """
        return dict_to_dimstring(self.dimensions, long_format)
    
    def dimOverlap(self, DIMS):
        """
        Check to see if a set of dimensions overlap at all with those of self
        """
        for dim in self.dimensions.keys():
            if dim in DIMS.keys():
                return True # we have some overlap
        
        return False
    
    def __str__(self):
        return 'generic entity'
    
    def __repr__(self):
        return self.__str__()

def dict_to_dimstring(dimdict, long_format=0):
    """
    Translate a dimension dict into a string.
            
    The long_format option will cause the dimensions to be spelled out as opposed to the abbreviations.
    """
    global DIMENSIONS

    if len(dimdict) == 0:
        return 'dimensionless'

    strings = []

    for dim in dimdict.keys():
        if dimdict[dim] == 1:
            # exponent of one, so
            strings.append(str(DIMENSIONS[dim][long_format]))
        elif dimdict[dim] < 0:
            # negative exponent, put in parentheses
            strings.append(str(DIMENSIONS[dim][long_format])+'^('+str(dimdict[dim])+')')
        else:
            # positive exponent, no parens needed
            strings.append(str(DIMENSIONS[dim][long_format])+'^'+str(dimdict[dim]))

    return ' '.join(strings)

class quantity(math_entity):
    """
    Physical quantity (vector or scalar) with units
    """
    def __init__(self, string = None, name='', values=[], error=[], units=None, dimensions=None):
        """
        Initialize quantity to given values

        WARNING: Giving values, error, and units through anything but string is not safe!
        """
        global DEFAULT_ERROR

        math_entity.__init__(self)  # run generic code
        
        if string != None:
            # they provided a string so see if it works
            self.translateString(string)

        else:
            if len(values) > 0:
                self.values = copy.copy(values)

                if len(error) == 0:
                    # provided values but no error
                    # set error to DEFAULT_ERROR times each value
                    self.error = copy.copy([])

                    for i in range(len(values)):
                        self.error.append(DEFAULT_ERROR * values[i])

                elif len(error) == 1:
                    # only provided one error, so it counts for all values
                    self.error = copy.copy([])

                    for i in range(len(values)):
                        self.error.append(error[0])

                elif len(error) != len(values):
                    # mismatch of values, error
                    raise exc('Length of initial error vector does not equal length of value vector.')

                else:
                    self.error = copy.copy(error)

            else:
                self.values = []
                self.error = []

            if units != None:
                self.units = copy.copy(units)

                if dimensions == None:
                    # supplied units but no dimensions!
                    possibledims = dimension_dict()

                    for unit in units.keys():
                        dims = getunitdims(unit)

                        for dim in dims.keys():
                            if dim in possibledims.keys():
                                possibledims[dim] += dims[dim] * units[unit]
                            else:
                                possibledims[dim] = dims[dim] * units[unit]

                            if possibledims[dim] == 0:
                                del possibledims[dim]

                    self.dimensions = copy.copy(possibledims)

                else:
                    # provided dimensions, we'll trust them here
                    self.dimensions = copy.copy(dimensions)
            else:
                self.units = {}
                self.dimensions = dimension_dict()

    def __copy__(self):
        """
        Return a copy of quantity.

        Note, here we make copies of any lists, tuples, or dictionaries then assign them
        to our resultant quantity so that we don't double-reference any containers
        """        
        # need to copy some extra stuff here
        valcpy = copy.deepcopy(self.values)
        errcpy = copy.deepcopy(self.error)
        unicpy = copy.deepcopy(self.units)
        dimcpy = copy.deepcopy(self.dimensions)

        return quantity(name=self.name, values=valcpy, error=errcpy, units=unicpy, dimensions=dimcpy)
    
    def valueString(self, value, error):
        """
        Format a single numerical value and error.
    
        @param value float value of quantity
        @param error float error (uncertainty) of quantity
    
        @return a string with rounded value and error (if not zero) in parentheses
        """
        string = ''
    
        if error == 0:
            string = str(value)
        else:
            logerror = int(math.floor(math.log10(error)))
    
            if logerror > 0:
                # need to round to an integer
                roundval = int(round(value * 10**(1 - logerror)) * 10**(logerror - 1))
    
                string += ('%(val)d') % {'val' : roundval}
            else:
                # we can just print to some precision
                if abs(value) > 0:
                    logvalue = int(math.floor(math.log10(abs(value))))
    
                    string += ("%(val)0." + str(abs(logvalue + 2 - logerror)) + "g") % {'val' : value}
                else:
                    string += ("%(val)0." + str(abs(2 - logerror)) + "g") % {'val' : value}
    
            rounderror = round(error * 10**(1 - logerror)) * 10**(logerror - 1)
    
            string += '(%(err)g)' % {'err' : rounderror}
    
        return string
    
    def translateString(self, string):
        """
        Translate string into quantity

        Returns True for success, False for invalid syntax
        Exceptions thrown on invalid numerical formats
        """
        global inf, DEFAULT_ERROR, MAXVECTORSIZE

        VALUE_STR = ''
        UNIT_STR = ''

        IN_UNIT = False
        IS_FINISHED = False # finished parsing?

        self.units = {}
        self.dimensions = dimension_dict()
        self.values = []
        self.error = []

        matchunits = re.match('^\s*[\+\-]?\s*(?P<valerr>.+)\s*<\s*(?P<units>.+)\s*>\s*$', string)

        if matchunits != None:
            # units were supplied
            VALUE_STR = matchunits.group('valerr').strip()
            UNIT_STR = matchunits.group('units').strip()
        else:
            # didn't give any units
            VALUE_STR = string.strip()

        if self.unitsFromString(UNIT_STR) == False:
            # units have incorrect syntax, so this is already bogus
            return False

        matchinf = re.search('^\s*[\+\-]?\s*inf\s*$', VALUE_STR, re.I)
        matchscalar = re.match('^\s*(?P<scalar>[-+]?\s*[\d\.\s]+(e[-+]?[\d\s]+)?)\s*(\((?P<error>[^\)]+)\))?\s*$', VALUE_STR, re.I)
        matchvector = re.match('^\s*\[(.+)\]\s*$', VALUE_STR)

        if matchinf != None:
            # infinity, no error
            if re.match('^\s*\-', VALUE_STR):
                # negative infinity
                self.values = [-inf]

            else:
                self.values = [inf]

            self.error = [0]   # only case of default no error

        elif matchscalar != None and  matchscalar.group('scalar') != None:
            # scalar format optionally with error
            try:
                self.values = [float(matchscalar.group('scalar'))]
            except ValueError:
                raise exc('Value string is not in valid numerical format.')

            if matchscalar.group('error') != None:
                # supplied something in parentheses
                try:
                    self.error = [float(matchscalar.group('error'))]
                except ValueError:
                    raise exc('Error string is not in valid numerical format.')

            else:
                # set default error
                if self.values[0] == 0:
                    # can't take log of 0
                    self.error = [DEFAULT_ERROR]
                else:
                    self.error = [DEFAULT_ERROR * 10 ** math.floor(math.log10(abs(self.values[0])))]

        elif matchvector != None:
            # explicit (VECTOR) format
            ELEMENTS = []       # holds elements of array as strings

            VALUE_STR = re.sub('(^\s*\[|\]\s*$)', '', VALUE_STR) # just get the insides
            
            PAREN_LEVEL = 0
            BRACKET_LEVEL = 0
            TMP = ''

            ELEMENTS = careful_split(',', VALUE_STR)
            
            self.dimensions = None # this is just so we can compare later dimensions
            for ELEMENT in ELEMENTS:
                # try to evaluate each string, need to check that all units match
                try:
                    elt = evaluate(ELEMENT) # evaluate string, may get vector w/ units, error
                    
                    if self.dimensions == None:
                        # dimensions/units not yet set
                        self.dimensions = elt.dimensions
                        self.units = elt.dimensions
                    else:
                        # dimensions set, check against them
                        if self.dimensions != elt.dimensions:
                            raise exc('Units of ' + ELEMENT + ' conflict with other elements in array.')
                        else:
                            # dimensions OK, convert to previous units before we add this to our values
                            elt.convert(self.units)
                    
                    for j in range(len(elt.values)):
                        self.values.append(elt.values[j])
                        self.error.append(elt.error[j])
                        
                except ValueError:
                    raise exc(ELEMENT + ' is not in valid numerical format!')
            
        else:
            # look for incremental vector assignment, then give up
            PAREN_LEVEL = 0
            TMP = ''
            BOUNDS_ARRAY = []

            for i in range(len(VALUE_STR)):
                if PAREN_LEVEL == 0 and VALUE_STR[i]== ':':
                    BOUNDS_ARRAY.append(TMP)

                    TMP = ''  # reset temp string

                else:
                    TMP += VALUE_STR[i]

                    if VALUE_STR[i] == '(':
                        PAREN_LEVEL += 1

                    elif VALUE_STR[i] == ')':
                        PAREN_LEVEL -= 1

                    if PAREN_LEVEL < 0:
                        # this is invalid syntax, so exit quietly
                        return False

            BOUNDS_ARRAY.append(TMP)

            if len(BOUNDS_ARRAY) < 2:
                # 0 or 1 elements, i.e. no semicolons, NOT GOOD!
                return False

            elif len(BOUNDS_ARRAY) == 2:
                # default increment = 1
                try:
                    START = float(BOUNDS_ARRAY[0])
                    END = float(BOUNDS_ARRAY[1])
                except ValueError:
                    raise exc('Bounds of incremental vector assignment not in valid numerical format!')


                if abs(END - START) > MAXVECTORSIZE - 1:
                    # this is a really big vector
                    raise exc('Vector may not contain more than ' + str(MAXVECTORSIZE) + ' elements!')

                if END < START:
                    # iterate backwards
                    INC = -1

                else:
                    INC = 1

                TMP_VAL = START     # start at START (obviously)

                while INC * TMP_VAL <= INC * END:
                    self.values.append(TMP_VAL)
                    self.error.append(DEFAULT_ERROR * 10 ** math.floor(math.log10(abs(TMP_VAL))))

                    TMP_VAL += INC

            elif len(BOUNDS_ARRAY) == 3:
                # they supply increment
                try:
                    START = float(BOUNDS_ARRAY[0])
                    INC = float(BOUNDS_ARRAY[1])
                    END = float(BOUNDS_ARRAY[2])
                except ValueError:
                    raise exc('Bounds or increment of incremental vector assignment not in valid numerical format!')

                if INC == 0:
                    raise exc('Increment may not be zero.')

                if abs((END - START) / INC) > MAXVECTORSIZE - 1:
                    # this is a really big vector
                    raise exc('Vector may not contain more than ' + str(MAXVECTORSIZE) + ' elements! (see conf.py)')

                if END < START:
                    DIRECTION = -1

                else:
                    DIRECTION = 1

                TMP_VAL = START

                while DIRECTION * TMP_VAL <= DIRECTION * END:
                    self.values.append(TMP_VAL)

                    try:
                        self.error.append(DEFAULT_ERROR * 10 ** math.floor(math.log10(abs(TMP_VAL))))
                    except OverflowError:
                        # catch math range error
                        self.error.append(0)

                    TMP_VAL += INC * DIRECTION

            else:
                # too many semi-colons
                raise exc('Too many semi-colons in incremental vector definition.')

        # successful translation if no exceptions by now
        return True

    def toBaseUnits(self):
        """
        Convert this quantity to a combination of base units
        """
        global UNITS

        # this will be our unit list when finished
        base = {}

        for unit, unitexp in self.units.iteritems():
            # loop through all units and convert individually
            if UNITS[unit].base_unit != 0:
                # derived unit

                # adjust values and error
                for i in range(len(self.values)):
                    self.values[i] = math.pow(math.pow(self.values[i], 1/unitexp) \
                        * UNITS[unit].to_base
                        + UNITS[unit].to_base_offset, unitexp)
                    self.error[i] *= math.pow(UNITS[unit].to_base, unitexp)

                # find base units for all dimensions
                for dim in UNITS[unit].dimensions:
                    bunit = findbase(dim)

                    if bunit in base:
                        base[bunit] += unitexp * UNITS[unit].dimensions[dim]
                        # clean up cancelled units
                        if base[bunit] == 0:
                            del base[bunit]
                    else:
                        base[bunit] = unitexp * UNITS[unit].dimensions[dim]

            else:
                # this is already a base unit, so add it to base and move on

                if UNITS[unit].base_dimension == 0:
                    # this is a combination of base units
                    for i in UNITS[unit].dimensions:
                        bunit = findbase(i)

                        if bunit in base:
                            base[bunit] += self.units[unit] * UNITS[unit].dimensions[i]
                            # clean up cancelled units
                            if base[bunit] == 0:
                                del base[bunit]
                        else:
                            base[bunit] = self.units[unit] * UNITS[unit].dimensions[i]
                else:
                    #this is a base unit and not a combination
                    if unit in base:
                        base[unit] += self.units[unit]
                        # clean up cancelled units
                        if base[bunit] == 0:
                            del base[bunit]
                    else:
                        base[unit] = self.units[unit]


        # all our values are converted, so set our unit list accordingly
        self.units = base

    def convert(self, toUnits):
        """
        Convert this quantity to given units
        """
        global UNITS

        # let's do a check on the dimensions first of all
        toDimensions = dimension_dict()
        for toUnit in toUnits:
            if len(UNITS[toUnit].dimensions) == 0:
                # base unit not combination
                if UNITS[toUnit].base_dimension in toDimensions:
                    toDimensions[UNITS[toUnit].base_dimension] += toUnits[toUnit]
                    # clean up cancelled dimensions
                    if toDimensions[UNITS[toUnit].base_dimension] == 0:
                        del toDimensions[UNITS[toUnit].base_dimension]
                else:
                    toDimensions[UNITS[toUnit].base_dimension] = toUnits[toUnit]

            else:
                for toDim in UNITS[toUnit].dimensions:
                    if toDim in toDimensions:
                        toDimensions[toDim] += toUnits[toUnit] * UNITS[toUnit].dimensions[toDim]
                        # clean up cancelled dimensions
                        if toDimensions[toDim] == 0:
                            del toDimensions[toDim]
                    else:
                        toDimensions[toDim] = toUnits[toUnit] * UNITS[toUnit].dimensions[toDim]

        # we now know what dimensions of toUnit are, check if they agree with ours
        if toDimensions != self.dimensions:
            raise exc('Cannot do conversion, dimensions do not match!')

        self.toBaseUnits()  # get us into base units first

        for TARGET_UNIT in toUnits:
            # loop through the target units
            # need to determine whether this is base unit or not
            TARGET_EXP = toUnits[TARGET_UNIT]

            if UNITS[TARGET_UNIT].base_unit != 0:
                # this is not a base unit, so it needs converting
                SCALE, OFFSET = UNITS[TARGET_UNIT].from_base, \
                                    UNITS[TARGET_UNIT].from_base_offset

                for i in range(len(self.values)):
                    # take TARGET_EXP'th root
                    self.values[i] = math.pow(self.values[i], 1/TARGET_EXP)

                    self.values[i] = SCALE * self.values[i] + OFFSET
                    self.error[i] *= math.pow(SCALE, TARGET_EXP)

                    self.values[i] = math.pow(self.values[i], TARGET_EXP)

        # if nothing goes wrong by now then all we have to do is update units
        self.units = toUnits

    def __str__(self):
        """
        Translate quantity into string
        """
        global UNITS

        quantstring = ''

        if len(self.values) == 1:
            # scalar
            VALSTR = self.valueString(self.values[0], self.error[0])
        elif len(self.values) > 10:
            # too big to print it all
            STRINGS1 = []

            for i in range(3):
                STRINGS1.append(self.valueString(self.values[i], self.error[i]))

            STRINGS2 = []

            for i in range(len(self.values) - 3, len(self.values)):
                STRINGS2.append(self.valueString(self.values[i], self.error[i]))

            VALSTR = '[' + ','.join(STRINGS1) + ', ... ,' + ','.join(STRINGS2) + ']'

        else:
            # regular vector
            STRINGS = []

            for i in range(len(self.values)):
                STRINGS.append(self.valueString(self.values[i], self.error[i]))

            VALSTR = '[' + ','.join(STRINGS) + ']'

        if len(self.units) == 0:
            # no units to print
            return VALSTR

        else:
            # let's print some units
            UNIT_STR_ARRAY = []

            for unit in self.units:
                if (self.units[unit] != 1):
                    UNIT_STR_ARRAY.append(UNITS[unit].abbrev + '^' \
                            + str(self.units[unit]))
                else:
                    UNIT_STR_ARRAY.append(UNITS[unit].abbrev)

            return str(VALSTR) + '<' + ' '.join(UNIT_STR_ARRAY) + '>'


    def unitsFromString(self, string):
        """
        Translate a string into units for this quantity

        Units are not combined in any way, but exponents of dimensions
        are additive. This means that dimensions are directly comparable
        but the string representation of a unit will look the same as how
        you assign it until it is changed.

        Return True on success, False on invalid syntax
        Exceptions thrown on nonsensical units or exponents
        """
        global UNITS

        self.units = {}   # start with empty array
        self.dimensions = dimension_dict()

        unitexpr = re.compile('\s*(?P<unit>.*)\^(?P<exp>.+)\s*$')
        for TERM in re.split('\s+', string):
            unitsmatch = unitexpr.match(TERM)

            if unitsmatch != None:
                # this has an exponent
                UNIT = unitsmatch.group('unit')
                try:
                    EXP = float(unitsmatch.group('exp'))
                except ValueError:
                    raise exc("Unit exponent '" + EXP + "' not in valid numerical format!")

            else:
                # no exponent given, default to 1
                UNIT = TERM
                EXP = 1.

            if UNIT != '':
                # ignore blank unit identifiers
                ID = findunit(UNIT)
                if ID != None:
                    # this is a known unit
                    self.units[ID] = EXP

                    if UNITS[ID].base_dimension != 0:
                        # base unit for the given dimension
                        if UNITS[ID].base_dimension in self.dimensions.keys():
                            self.dimensions[UNITS[ID].base_dimension] += EXP

                        else:
                            self.dimensions[UNITS[ID].base_dimension] = EXP

                    else:
                        for UNIT_DIM in UNITS[ID].dimensions.keys():
                            UNIT_DIM_EXP = UNITS[ID].dimensions[UNIT_DIM]

                            if self.dimensions.has_key(UNIT_DIM):
                                self.dimensions[UNIT_DIM] += UNIT_DIM_EXP * EXP

                            else:
                                self.dimensions[UNIT_DIM] = UNIT_DIM_EXP * EXP

                else:
                    raise exc("Unknown Unit: '" + UNIT + "'\n")

        # make dimensions look ok (get rid of zeros)
        for i in self.dimensions.keys():
            if self.dimensions[i] == 0:
                del self.dimensions[i]

        return True

    def __getitem__(self, key):
        """
        Retrieve a portion of the vector.
        """
        if key.__class__ == types.SliceType:
            val = self.values[key.start:key.stop]
            err = self.error[key.start:key.stop]
        elif key.__class__ == types.IntType:
            val = [self.values[key]]
            err = [self.error[key]]
        else:
            raise exc('Invalid index type')

        return quantity(values=val,error=err, \
            dimensions=copy.deepcopy(self.dimensions), \
            units=copy.deepcopy(self.units))

    def __len__(self):
        """
        Simply returns size of vector
        """
        return len(self.values)

    def __cmp__(self, quant):
        """
        Compare to another quantity.

        Note that a successful comparison occurs whenever the uncertainties overlap
        """
        if len(self) > 1 or len(quant) > 1:
            raise exc('Ordering not defined for vector quantities.')

        if self.dimensions != quant.dimensions:
            raise exc('Dimensions must agree for quantity comparison.')

        # make a couple copies so we can safely convert them to base units then compare them
        A = copy.copy(self)
        B = copy.copy(quant)

        A.toBaseUnits()
        B.toBaseUnits()

        for i in range(len(A.values)):
            if A.values[i] - A.error[i] < B.values[i] + B.error[i] and \
                B.values[i] - B.error[i] < A.values[i] + A.error[i]:
                continue

            if B.values[i] - B.error[i] < A.values[i] + A.error[i] and \
                A.values[i] - A.error[i] < B.values[i] + B.error[i]:
                continue

            else:
                return False

        return True

    def __eq__(self, quant):
        """
        Alias of __cmp__()
        """
        return self.__cmp__(quant)

    def __ne__(self, quant):
        """
        Negation of __eq__()
        """
        return not self.__eq__(quant)

    def __lt__(self, quant):
        """
        Less than operator

        Returns True if A.value + error strictly less than B.value - error
        with no overlap
        """
        if len(self) > 1 or len(quant) > 1:
            raise exc('Ordering not defined for vector quantities.')

        if self.dimensions != quant.dimensions:
            raise exc('Dimensions must agree for quantity comparison.')

        # make a couple copies so we can safely convert them to base units then compare them
        A = copy.deepcopy(self)
        B = copy.deepcopy(quant)

        A.toBaseUnits()
        B.toBaseUnits()

        for i in range(len(A.values)):
            if A.values[i] + A.error[i] > B.values[i] - B.error[i]:
                # A + error >= B - error
                return False

        return True

    def __le__(self, quant):
        """
        Less than or equal operator

        Returns True if all A.value + error is less than B.value - error
        or if the two quantities are equal
        """
        if len(self) > 1 or len(quant) > 1:
            raise exc('Ordering not defined for vector quantities.')

        if self.dimensions != quant.dimensions:
            raise exc('Dimensions must agree for quantity comparison.')

        # make a couple copies so we can safely convert them to base units then compare them
        A = copy.deepcopy(self)
        B = copy.deepcopy(quant)

        A.toBaseUnits()
        B.toBaseUnits()

        for i in range(len(A.values)):
            if A.values[i] - A.error[i] < B.values[i] + B.error[i] and \
                B.values[i] - B.error[i] < A.values[i] + A.error[i]:
                continue

            if B.values[i] - B.error[i] < A.values[i] + A.error[i] and \
                A.values[i] - A.error[i] < B.values[i] + B.error[i]:
                continue

            else:
                return False

        return True

    # TODO: implement comparison operators for quantity objects

    def __add__(self, B):
        """
        Add entity to self

        Note that addition of two vectors is allowed only if they have equal length
        Also, addition of a scalar to a vector __is__ allowed, in which case the
        scalar is treated as a vector of same length as other vector with all
        elements equal to the scalar's value

        error(A+B) = sqrt(error(A)**2 + error(B)**2)
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
        
        # everything has dimensions, so we can check this first
        if self.dimensions != B.dimensions:
            raise exc('Dimensions of entities must match for addition operation!')
        
        if B.__class__ == symbol:
            # quantity + symbol
            return function(dimensions=self.dimensions,\
                arguments = (B.name,),\
                data=(const.TYPE_ARITH, const.OP_ADD,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # quantity + function
            return function(dimensions=self.dimensions,\
                arguments = B.arguments,\
                data=(const.TYPE_ARITH, const.OP_ADD,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    B.data))
        elif B.__class__ == quantity:
            if len(self.values) != len(B.values) \
                    and len(self.values) != 1 \
                    and len(B.values) != 1:
                raise exc('Attempting to add vectors of different lengths!')
    
            selfcopy = copy.deepcopy(self)     # work with this copy so we don't change self
    
            if selfcopy.units != B.units:
                # convert B (or a copy) to units of self
                selfcopy.convert(B.units)
    
            out = quantity(dimensions = selfcopy.dimensions, units = selfcopy.units)
    
            if len(selfcopy.values) == 1:
                # self is a scalar
                for i in range(len(B.values)):
                    out.values.append(selfcopy.values[0] + B.values[i])
                    out.error.append(math.sqrt(selfcopy.error[0]**2 + B.error[i]**2))
            elif len(B.values) == 1:
                # B is a scalar
                for i in range(len(selfcopy.values)):
                    out.values.append(selfcopy.values[i] + B.values[0])
                    out.error.append(math.sqrt(selfcopy.error[i]**2 + B.error[0]**2))
            else:
                # adding two vectors
                for i in range(len(selfcopy.values)):
                    out.values.append(selfcopy.values[i] + B.values[i])
                    out.error.append(math.sqrt(selfcopy.error[i]**2 + B.error[i]**2))
    
            return out

    def __sub__(self, B):
        """
        Subtract two entities

        Note that subtraction of two vectors is allowed only if they have equal
        length. Also, subtraction of a scalar to a vector __is__ allowed, in
        which case the scalar is treated as a vector of same length as other
        vector with all elements equal to the scalar's value

        error(A+B) = sqrt(error(A)**2 + error(B)**2)
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
        
        # everything has dimensions, so we can check this first
        if self.dimensions != B.dimensions:
            raise exc('Dimensions of entities must match for subtraction operation!')
        
        if B.__class__ == symbol:
            # quantity - symbol
            return function(dimensions=self.dimensions,\
                arguments = (B.name,),\
                data=(const.TYPE_ARITH, const.OP_SUB,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # quantity - function
            return function(dimensions=self.dimensions,\
                arguments = B.arguments,\
                data=(const.TYPE_ARITH, const.OP_SUB,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    B.data))
        elif B.__class__ == quantity:
            if len(self.values) != len(B.values) \
                    and len(self.values) != 1 \
                    and len(B.values) != 1:
                raise exc('Attempting to add vectors of different lengths!')
    
            selfcopy = copy.deepcopy(self)     # work with this copy so we don't change self
    
            if selfcopy.units != B.units:
                # convert B (or a copy) to units of self
                selfcopy.convert(B.units)
    
            out = quantity(dimensions = copy.copy(selfcopy.dimensions), units = copy.copy(selfcopy.units))
    
            if len(selfcopy.values) == 1:
                # self is a scalar
                for i in range(len(B.values)):
                    out.values.append(selfcopy.values[0] - B.values[i])
                    out.error.append(math.sqrt(selfcopy.error[0]**2 + B.error[i]**2))
            elif len(B.values) == 1:
                # B is a scalar
                for i in range(len(selfcopy.values)):
                    out.values.append(selfcopy.values[i] - B.values[0])
                    out.error.append(math.sqrt(selfcopy.error[i]**2 + B.error[0]**2))
            else:
                # subtracting two vectors
                for i in range(len(selfcopy.values)):
                    out.values.append(selfcopy.values[i] - B.values[i])
                    out.error.append(math.sqrt(selfcopy.error[i]**2 + B.error[i]**2))
    
            return out

    def __mul__(self, B):
        """
        Multiply self by entity

        This will do component-wise multiplication of vectors if they have equal length,
        otherwise simple scalar multiplication

        error(A*B) = (A*B)*sqrt((error(A)/A)**2 + (error(B)/B)**2)
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')

        if B.__class__ == symbol:
            # quantity * symbol
            return function(dimensions=self.dimensions * B.dimensions,\
                arguments = (B.name,),\
                data=(const.TYPE_ARITH, const.OP_MUL,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # quantity * function
            return function(dimensions=self.dimensions * B.dimensions,\
                arguments = B.arguments,\
                data=(const.TYPE_ARITH, const.OP_MUL,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    B.data))
        elif B.__class__ == quantity:
            # quantity * quantity
            if len(self.values) > 1 and len(B.values) > 1 and \
                len(A.values) != len(B.values):
                raise exc("Size of two vectors must be equal for multiplication");
    
            selfcopy = copy.deepcopy(self)     # work with a copy so we can convert units
    
            OUT = quantity()    # this is our output
    
            # if dimensions overlap or (temperature is involved) then go to base units
            if selfcopy.dimOverlap(B.dimensions) or \
                4 in selfcopy.dimensions.keys() or 4 in B.dimensions.keys():
                selfcopy.toBaseUnits()
                B.toBaseUnits()
    
            OUT.units = copy.copy(selfcopy.units)
            OUT.dimensions = copy.copy(selfcopy.dimensions)
    
            if len(selfcopy.values) == 1:
                # self is a scalar
                for i in range(len(B.values)):
                    OUT.values.append(selfcopy.values[0] * B.values[i])
    
                    if selfcopy.values[0] == 0 or B.values[i] == 0:
                        OUT.error.append(0)
    
                    else:
                        OUT.error.append(abs(OUT.values[i]) * math.sqrt((selfcopy.error[0] / selfcopy.values[0])**2 + (B.error[i] / B.values[i])**2))    # dC = (A*B)*sqrt((dA/A)^2 + (dB/B)^2)
    
            elif len(B.values) == 1:
                # quant is a scalar
                for i in range(len(selfcopy.values)):
                    OUT.values.append(B.values[0] * selfcopy.values[i])
    
                    if selfcopy.values[i] == 0 or B.values[0] == 0:
                        OUT.error.append(0)
    
                    else:
                        OUT.error.append(abs(OUT.values[i]) * math.sqrt((selfcopy.error[i] / selfcopy.values[i])**2 + (B.error[0] / B.values[0])**2))
    
            else:
                for i in range(len(selfcopy.values)):
                    OUT.values.append(selfcopy.values[i] * B.values[i])
    
                    if selfcopy.values[i] == 0 or B.values[i] == 0:
                        OUT.error.append(0)
    
                    else:
                        OUT.error.append(abs(OUT.values[i]) * math.sqrt((selfcopy.error[i] / selfcopy.values[i])**2 + (B.error[i] / B.values[i])**2))
    
    
            for UNIT in B.units.keys():
                # all are base units so we can just combine like crazy
                if OUT.units.has_key(UNIT):
                    OUT.units[UNIT] += B.units[UNIT]
    
                else:
                    OUT.units[UNIT] = B.units[UNIT]
    
            for DIM in B.dimensions.keys():
                if OUT.dimensions.has_key(DIM):
                    OUT.dimensions[DIM] += B.dimensions[DIM]
    
                else:
                    OUT.dimensions[DIM] = B.dimensions[DIM]
    
            OUT.prettyUnits()   # clean up the units and make her look nice
    
            return OUT

    def __div__(self, B):
        """
        Divide two quantities

        This will do component-wise division of vectors if they have equal length,
        otherwise simple scalar multiplication

        NOTE: Division by zero now raises an exception, since 0/0 is undefined
        Likewise inf/inf or inf/-inf is also undefined, but a/inf = 0 for |a| != inf

        error(A/B) = (A/B)*sqrt((error(A)/A)**2 + (error(B)/B)**2)
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')

        if B.__class__ == symbol:
            # quantity * symbol
            return function(dimensions=self.dimensions / B.dimensions,\
                arguments = (B.name,),\
                data=(const.TYPE_ARITH, const.OP_DIV,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # quantity / function
            return function(dimensions=self.dimensions / B.dimensions,\
                arguments = B.arguments,\
                data=(const.TYPE_ARITH, const.OP_DIV,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    B.data))
        elif B.__class__ == quantity:
            # quantity / quantity
            if len(self.values) > 1 and len(B.values) > 1 and \
                len(A.values) != len(B.values):
                raise exc("Size of two vectors must be equal for multiplication");
    
            selfcopy = copy.deepcopy(self)     # work with a copy so we can convert units
    
            OUT = quantity()    # this is our output
    
            # if dimensions overlap or (temperature is involved) then go to base units
            if selfcopy.dimOverlap(B.dimensions) or \
                4 in selfcopy.dimensions.keys() or 4 in B.dimensions.keys():
                selfcopy.toBaseUnits()
                B.toBaseUnits()
    
            OUT.units = selfcopy.units
            OUT.dimensions = selfcopy.dimensions
    
            if len(selfcopy.values) == 1:
                # self is a scalar
                for i in range(len(B.values)):
                    OUT.values.append(selfcopy.values[0] / B.values[i])
    
                    if B.values[i] == 0.:
                        # division by zero
                        raise exc('Division by zero not allowed.')
    
                    elif abs(self.values[0]) == inf and abs(B.values[i]) == inf:
                        raise exc('inf/inf not defined.')
    
                    else:
                        OUT.error.append(abs(OUT.values[i]) * math.sqrt((selfcopy.error[0] / selfcopy.values[0])**2 + (B.error[i] / B.values[i])**2))    # dC = (A/B)*sqrt((dA/A)^2 + (dB/B)^2)
    
            elif len(B.values) == 1:
                # quant is a scalar
                for i in range(len(selfcopy.values)):
                    OUT.values.append(selfcopy.values[i] / B.values[0])
    
                    if B.values[0] == 0.:
                        # division by zero
                        raise exc('Division by zero not allowed.')
    
                    elif abs(self.values[i]) == inf and abs(B.values[0]) == inf:
                        raise exc('inf/inf not defined.')
    
                    else:
                        OUT.error.append(abs(OUT.values[i]) * math.sqrt((selfcopy.error[i] / selfcopy.values[i])**2 + (B.error[0] / B.values[0])**2))
    
            else:
                for i in range(len(selfcopy.values)):
                    OUT.values.append(selfcopy.values[i] / B.values[i])
    
                    if B.values[i] == 0:
                        OUT.error.append(0)
    
                    elif abs(self.values[i]) == inf and abs(B.values[i]) == inf:
                        raise exc('inf/inf not defined.')
    
                    else:
                        OUT.error.append(abs(OUT.values[i]) * math.sqrt((selfcopy.error[i] / selfcopy.values[i])**2 + (B.error[i] / B.values[i])**2))
    
    
            for UNIT in B.units.keys():
                # all are base units so we can just combine like crazy
                if OUT.units.has_key(UNIT):
                    OUT.units[UNIT] -= B.units[UNIT]
    
                else:
                    OUT.units[UNIT] = -B.units[UNIT]
    
            for DIM in B.dimensions.keys():
                if OUT.dimensions.has_key(DIM):
                    OUT.dimensions[DIM] -= B.dimensions[DIM]
    
                else:
                    OUT.dimensions[DIM] = -B.dimensions[DIM]
    
            OUT.prettyUnits()   # clean up the units and make her look nice
    
            return OUT

    def __pow__(self, B):
        """
        Raise quantity to some exponent

        The exponent must be a dimensionless scalar.

        error(A^B) = (A^B)*sqrt((B/A * dA)^2 + (ln(A)*dB)^2)
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')

        if len(B.dimensions) > 0:
            raise exc('Exponent must be dimensionless for exponentiation.')

        if B.__class__ == symbol:
            # quantity ^ symbol
            if len(self.dimensions) > 0:
                raise exc('Exponentiation to symbolic power is only possible for dimensionless entities.')
            return function(arguments = (B.name,),\
                data=(const.TYPE_ARITH, const.OP_EXP,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # quantity ^ function
            if len(self.dimensions) > 0:
                raise exc('Exponentiation to symbolic power is only possible for dimensionless entities.')
            return function(arguments = B.arguments,\
                data=(const.TYPE_ARITH, const.OP_EXP,\
                    (const.TYPE_VAR, copy.deepcopy(self)),\
                    B.data))
        elif B.__class__ == quantity:
            # quantity ^ quantity
            if len(B.values) != 1:
                raise exc('Exponent must be a scalar.')
    
            OUT = quantity()
    
            # single exponent, just raise each element of self to this
            for i in range(len(self.values)):
                if self.values[i] < 0 and round(B.values[0]) != B.values[0]:
                    raise exc('Cannot raise negative number to non-integer power.')
    
                if abs(self.values[i]) == inf:
                    # avoid math range errors
                    if B.values[0] == 0:
                        OUT.values.append(1)
    
                    elif B.values[0] < 0:
                        OUT.values.append(0)
    
                    else:
                        if self.values[i] < 0:
                            OUT.values.append((-1)**B.values[0]*inf)
    
                        else:
                            OUT.values.append(self.values[i])
    
                    OUT.error.append(0)
    
                elif self.values[i] == 0:
                    # error 0 for 0^B
                    OUT.values.append(self.values[i]**B.values[0])
                    OUT.error.append(0)
    
                else:
                    OUT.values.append(self.values[i]**B.values[0])
                    OUT.error.append(abs(OUT.values[i] * \
                        math.sqrt((self.error[i] * B.values[0] / self.values[i])**2 + \
                        (math.log(abs(self.values[i])) * B.error[0])**2)))
    
            for UNIT in self.units.keys():
                #raise all units to the power of B
                OUT.units[UNIT] = self.units[UNIT] * B.values[0]
    
            OUT.prettyUnits()      # polish her up
    
            return OUT

    def prettyUnits(self):
        """
        Make the units pretty (i.e. kg K^0 m^2 A^0 s^-2 -> J)

        TODO: combinations of units into derived units
        """

        for i in self.units.keys():
            # prune out units^0
            if self.units[i] == 0:
                # unit^0
                del self.units[i]

        for i in self.dimensions.keys():
            # same as above
            if self.dimensions[i] == 0:
                del self.dimensions[i]


def execute(string, outputcallback= None, kwcallback = None):
    """
    Execute a block of puny code. Handles flow control.

    This evaluates a block of puny code. It handles flow control, variable assignment
    and keywords (if the kwcallback function argument is given). This must be defined like:

    >>> def keyword(name, args):

    Here, name is the name of the requested keyword and args is a list of the remaining words
    in the statement. This function must execute the keyword if it is valid and return True,
    otherwise it should return False.
    ***NOTE: This function must return a list of keyword names if name='' ***

    The outputcallback argument may be given to facilitate output from some keywords and
    functions. This function must be defined like so:

    >>> def output(string):

    Throws appropriate exception on error

    @param string block of puny code
    @param outputcallback output function object
    @param kwcallback keyword execution function object

    @return None
    """

    cmds = careful_split(';', string)

    for i in range(len(cmds)):
        cmds[i] = cmds[i].strip()

        if cmds[i] == '':
            # empty command, just skip
            # this takes care of semi-colons at the end of lines also
            continue

        kwmatch = re.match('\s*(?P<keyword>\w+)\s*(?P<argstr>.*)$', string)
        if kwcallback != None and kwmatch != None and kwcallback(kwmatch.group('keyword'), kwmatch.group('argstr')):
            # executed a keyword
            return

        else:
            # not a keyword, this is math

            # Here we simultaneously remove some whitespace and search for variable name assignment
            assignmatch = re.match('\s*((?P<ws>[^\s\.]*)\.)?(?P<varname>\S*)\s*=\s*(?P<statement>.*)\s*$', cmds[i])

            if assignmatch != None:
                # command is requesting assignment

                # here we should see if they listed a workspace name as well
                ws = assignmatch.group('ws')
                varname = assignmatch.group('varname')
                statement = assignmatch.group('statement')
                
                OUT = evaluate(statement)
                # try to set name, or fail
                OUT.setName(varname)

                if ws == None:
                    # did not provide target workspace
                    targetws = DEFAULTWS
                else:
                    targetws = ws.upper()
                   
                if not targetws in WORKSPACES.keys():
                    raise exc('Workspace '+targetws+' does not exist!')

                if OUT.name in pmath.FUNCTIONS.keys() or (kwcallback != None and OUT.name in kwcallback('','')):
                    # reserved name
                    raise exc('The variable name ' + OUT.name + ' is reserved for a function or keyword.')
                else:
                    # new variable name or updating workspace
                    WORKSPACES[targetws].setMember(OUT)

            else:
                # not a direct assignment
                OUT = evaluate(cmds[i])
                
                if OUT.name == '':
                    # if this quantity has a name already, then it is probably not an assignment at all
                    OUT.setName('ans')    # default variable name
                    WORKSPACES[DEFAULTWS].setMember(OUT)    # write to default workspace

            if i == len(cmds)-1:
                # print this result
                if type(outputcallback) is types.FunctionType or types.MethodType:
                    outputcallback("\t" + OUT.formalName() + " = " + str(OUT))

class symbol(math_entity):
    """
    Symbolic variable - placeholder with only units and dimensions, no value
    """
    def __add__(self, B):
        """
        Add entity to self.
        
        Note this _ALWAYS_ returns a function object.
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
        
        # everything has dimensions, so we can check this first
        if self.dimensions != B.dimensions:
            raise exc('Dimensions of entities must match for addition operation!')
        
        if B.__class__ == quantity:
            # symbol + quantity
            return function(dimensions=self.dimensions,\
                arguments = (self,),\
                data=(const.TYPE_ARITH, const.OP_ADD,\
                    (const.TYPE_SYM, self), \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == function:
            # symbol + function
            return function(dimensions=self.dimensions,\
                arguments = combine_args((self,), B.arguments),\
                data=(const.TYPE_ARITH, const.OP_ADD,\
                    (const.TYPE_SYM, self),\
                    B.data))
        elif B.__class__ == symbol:
            # symbol + symbol
            # Below is an ugly way to remove dupes
            return function(dimensions = self.dimensions,\
                arguments = (self, B), \
                data = (const.TYPE_ARITH, const.OP_ADD, \
                    (const.TYPE_SYM, self), \
                    (const.TYPE_SYM, B)))
        
    def __sub__(self, B):
        """
        Subtract entity from self.
        
        Note this _ALWAYS_ returns a function object.
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
        
        # everything has dimensions, so we can check this first
        if self.dimensions != B.dimensions:
            raise exc('Dimensions of entities must match for subtraction operation!')
        
        if B.__class__ == quantity:
            # symbol + quantity
            return function(dimensions=self.dimensions,\
                arguments = (self,),\
                data=(const.TYPE_ARITH, const.OP_SUB,\
                    (const.TYPE_SYM, self), \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == function:
            # symbol + function
            return function(dimensions=self.dimensions,\
                arguments = combine_args((self,), B.arguments),\
                data=(const.TYPE_ARITH, const.OP_SUB,\
                    (const.TYPE_SYM, self),\
                    B.data))
        elif B.__class__ == symbol:
            # symbol + symbol
            # Below is an ugly way to remove dupes
            return function(dimensions = self.dimensions,\
                arguments = (self, B), \
                data = (const.TYPE_ARITH, const.OP_SUB, \
                    (const.TYPE_SYM, self), \
                    (const.TYPE_SYM, B)))
                    
    def __mul__(self, B):
        """
        Multiply self by entity.
        
        Note this _ALWAYS_ returns a function object.
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
                
        if B.__class__ == quantity:
            # symbol + quantity
            return function(dimensions=self.dimensions * B.dimensions,\
                arguments = (self,),\
                data=(const.TYPE_ARITH, const.OP_MUL,\
                    (const.TYPE_SYM, self), \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == function:
            # symbol + function
            return function(dimensions=self.dimensions * B.dimensions,\
                arguments = combine_args((self,), B.arguments),\
                data=(const.TYPE_ARITH, const.OP_MUL,\
                    (const.TYPE_SYM, self),\
                    B.data))
        elif B.__class__ == symbol:
            # symbol + symbol
            # Below is an ugly way to remove dupes
            return function(dimensions = self.dimensions * B.dimensions,\
                arguments = (self, B), \
                data = (const.TYPE_ARITH, const.OP_MUL, \
                    (const.TYPE_SYM, self), \
                    (const.TYPE_SYM, B)))
    
    def __div__(self, B):
        """
        Divide self by entity.
        
        Note this _ALWAYS_ returns a function object.
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
                
        if B.__class__ == quantity:
            # symbol + quantity
            return function(dimensions=self.dimensions / B.dimensions,\
                arguments = (self,),\
                data=(const.TYPE_ARITH, const.OP_DIV,\
                    (const.TYPE_SYM, self), \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == function:
            # symbol + function
            return function(dimensions=self.dimensions / B.dimensions,\
                arguments = combine_args((self,), B.arguments),\
                data=(const.TYPE_ARITH, const.OP_DIV,\
                    (const.TYPE_SYM, self),\
                    B.data))
        elif B.__class__ == symbol:
            # symbol + symbol
            # Below is an ugly way to remove dupes
            return function(dimensions = self.dimensions / B.dimensions,\
                arguments = (self, B), \
                data = (const.TYPE_ARITH, const.OP_DIV, \
                    (const.TYPE_SYM, self), \
                    (const.TYPE_SYM, B)))

    def __pow__(self, B):
        """
        Raise self to power entity.
        
        Note this _ALWAYS_ returns a function object.
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
                
        if len(B.dimensions) > 0:
            raise exc('Exponent must be dimensionless for exponentiation.')
            
        if B.__class__ == quantity:
            # symbol ^ quantity
            if len(B.values) != 1:
                raise exc('Exponent must be a scalar.')

            return function(dimensions=self.dimensions ** B.values,\
                arguments = (self,),\
                data=(const.TYPE_ARITH, const.OP_EXP,\
                    (const.TYPE_SYM, self), \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == function:
            # symbol ^ function
            if len(self.dimensions) > 0:
                raise exc('Exponentiation to symbolic power is only possible for dimensionless entities.')
            return function(arguments = combine_args((self,), B.arguments),\
                data=(const.TYPE_ARITH, const.OP_EXP,\
                    (const.TYPE_SYM, self),\
                    B.data))
        elif B.__class__ == symbol:
            # symbol ^ symbol
            # Below is an ugly way to remove dupes
            if len(self.dimensions) > 0:
                raise exc('Exponentiation to symbolic power is only possible for dimensionless entities.')
            return function(arguments = (self, B), \
                data = (const.TYPE_ARITH, const.OP_EXP, \
                    (const.TYPE_SYM, self), \
                    (const.TYPE_SYM, B)))

    def addQuantity(self, Q):
        """
        Add a quantity (variable) to this symbol, returning a function
        """
        
    def addFunction(self, f):
        """
        Add a function to this symbol, returning another function
        """
        
    def __str__(self):
        """
        Return string representation of symbol.
        """
        if self.dimensions != None and len(self.dimensions) > 0:
            return 'symbol <'+self.dimString()+'>'
        else:
            return 'symbol <dimensionless>'
    
class function(math_entity):
    """
    A function is a nested tuple, with key the type of operation at that level (arithmetic,function,variable,symbol)
    and the values a tuple of arguments
    
    Example:
    a*cos(b)+4
    is stored as something like
    (TYPE_ARITH,OP_ADD,(TYPE_ARITH,OP_MUL,(TYPE_SYM, 'a'),(TYPE_FUNC,'cos',(TYPE_SYM,'b')))),(TYPE_CONST,quantity('',4))
    
    Types:
        TYPE_ARITH: arithmetic
            arguments = operation, A, B
            (operation is one of OP_ADD, OP_SUB, OP_MUL, OP_DIV, OP_EXP)
        TYPE_FUNC: function
            arguments = function name, tuple of f'n args
        TYPE_SYM: symbol (no nesting)
            arguments = symbol object
        TYPE_VAR: variable (no nesting)
            arguments = quantity object
        TYPE_CONST: constant (no nesting)
            arguments = constant name
    """
    def __init__(self, name='', string = None, dimensions = dimension_dict(), arguments = None, data = None):
        math_entity.__init__(self, name=name, dimensions=dimensions)  # run generic code
        
        if string != None:
            # providing string
            pass
        else:
            # either providing data or using default blank function
            self.arguments = arguments
            self.data = data

    # TODO: check which variables need to be copied in all these arithmetic methods
    def __add__(self, B):
        """
        Add entity to self
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
        
        # everything has dimensions, so we can check this first
        if self.dimensions != B.dimensions:
            raise exc('Dimensions of entities must match for addition operation!')
        
        if B.__class__ == quantity:
            # function + quantity
            return function(dimensions=self.dimensions,\
                arguments = self.arguments,\
                data=(const.TYPE_ARITH, const.OP_ADD,\
                    self.data, \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == symbol:
            # function + symbol
            return function(dimensions=self.dimensions,\
                arguments = combine_args(self.arguments,(B,)),\
                data=(const.TYPE_ARITH, const.OP_ADD,\
                    self.data,\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # function + function            
            return function(dimensions = self.dimensions,\
                arguments = combine_args(self.arguments, B.arguments),\
                data = (const.TYPE_ARITH, const.OP_ADD, self.data, B.data))

    def __sub__(self, B):
        """
        Subtract entity from self
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
        
        # everything has dimensions, so we can check this first
        if self.dimensions != B.dimensions:
            raise exc('Dimensions of entities must match for subtraction operation!')
        
        if B.__class__ == quantity:
            # function + quantity
            return function(dimensions=self.dimensions,\
                arguments = self.arguments,\
                data=(const.TYPE_ARITH, const.OP_SUB,\
                    self.data, \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == symbol:
            # function + symbol
            return function(dimensions=self.dimensions,\
                arguments = combine_args(self.arguments,(B,)),\
                data=(const.TYPE_ARITH, const.OP_SUB,\
                    self.data,\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # function + function            
            return function(dimensions = self.dimensions,\
                arguments = combine_args(self.arguments, B.arguments),\
                data = (const.TYPE_ARITH, const.OP_SUB, self.data, B.data))

    def __mul__(self, B):
        """
        Multiply entity by self
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
                
        if B.__class__ == quantity:
            # function + quantity
            return function(dimensions=self.dimensions * B.dimensions,\
                arguments = self.arguments,\
                data=(const.TYPE_ARITH, const.OP_MUL,\
                    self.data, \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == symbol:
            # function + symbol
            return function(dimensions=self.dimensions * B.dimensions,\
                arguments = combine_args(self.arguments,(B,)),\
                data=(const.TYPE_ARITH, const.OP_MUL,\
                    self.data,\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # function + function            
            return function(dimensions = self.dimensions * B.dimensions,\
                arguments = combine_args(self.arguments, B.arguments),\
                data = (const.TYPE_ARITH, const.OP_MUL, self.data, B.data))
                
    def __div__(self, B):
        """
        Divide self by entity
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
                
        if B.__class__ == quantity:
            # function + quantity
            return function(dimensions=self.dimensions / B.dimensions,\
                arguments = self.arguments,\
                data=(const.TYPE_ARITH, const.OP_DIV,\
                    self.data, \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == symbol:
            # function + symbol
            return function(dimensions=self.dimensions / B.dimensions,\
                arguments = combine_args(self.arguments, (B,)),\
                data=(const.TYPE_ARITH, const.OP_DIV,\
                    self.data,\
                    (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # function + function            
            return function(dimensions = self.dimensions / B.dimensions,\
                arguments = combine_args(self.arguments, B.arguments),\
                data = (const.TYPE_ARITH, const.OP_DIV, self.data, B.data))
    
    def __pow__(self, B):
        """
        Raise self to power B
        """
        if not B.__class__ in [quantity, function, symbol]:
            raise exc(B.name+' is of unrecognized type for this operation.')
        
        if len(B.dimensions) > 0:
            raise exc('Exponent must be dimensionless for exponentiation.')
        
        if B.__class__ == quantity:
            # function + quantity
            if len(B.values) != 1:
                raise exc('Exponent must be a scalar.')

            return function(dimensions=self.dimensions ** B.values[0],\
                arguments = self.arguments,\
                data=(const.TYPE_ARITH, const.OP_EXP,\
                    self.data, \
                    (const.TYPE_VAR, B)))
        elif B.__class__ == symbol:
            # function + symbol
            if len(self.dimensions) > 0:
                raise exc('Exponentiation to symbolic power is only possible for dimensionless entities.')
            return function(arguments = combine_args(self.arguments,(B,)),\
                data=(const.TYPE_ARITH, const.OP_EXP, self.data, (const.TYPE_SYM, B)))
        elif B.__class__ == function:
            # function + function
            if len(self.dimensions) > 0:
                raise exc('Exponentiation to symbolic power is only possible for dimensionless entities.')
            return function(arguments = combine_args(self.arguments, B.arguments),\
                data = (const.TYPE_ARITH, const.OP_EXP, self.data, B.data))

    def formalName(self):
        """
        Return the formal reference for this object.
        
        Lists the function name along with all its arguments
        """
        ARGNAMES = []
        
        for arg in self.arguments:
            # figure out what the dimensions are of each argument
            if len(arg.dimensions) == 0:
                # dimensionless, just don't mention anything
                ARGNAMES.append(arg.name)
            else:
                # put dimensions in name string
                ARGNAMES.append(arg.name+'<'+arg.dimString(0)+'>')
        
        if len(self.dimensions) > 0:
            return self.name+'('+','.join(ARGNAMES)+')<'+self.dimString(0)+'>'
        else:
            return self.name+'('+','.join(ARGNAMES)+')'
        
    def __str__(self):
        """
        Return string representation of symbol.
        """
        return self.dataString()
    
    def simplify(self):
        """
        Simplify expression
        """
        # TODO: write a simplify function which simplifies the expression.
        # might even simplify to a quantity or symbol
        # then need a recursive function which checks for all variable names and updates self.arguments

    def findArgs(self, data = None):
        """
        Update argument list
        """
        if data == None:
            # No data tuple provided, i.e. not recursing so return self data
            data = self.data
            
        # recurse and update a list of arguments, then pass to combine_args for sorting
    
    def eval(self, argdict, data = None):
        """
        Evaluate function with given named arguments
        
        Recursively inserts quantities for symbols and returns appropriate entity
        
        @param argdict dict of 'argname':quantity pairs
        """
        if data == None:
            # No data tuple provided, i.e. not recursing so return self data
            data = self.data
        # write eval function

    def dataString(self, data = None):
        """
        Translate data tuple into string representing function
        
        This is only outside the class because it needs to be recursive
        """
        if data == None:
            # No data tuple provided, i.e. not recursing so return self data
            data = self.data
        elif data.__class__ != tuple:
            # nonsense argument
            raise exc('Can only parse tuple into string for function translation.')
        
        if data[0] == const.TYPE_CONST:
            # physical constant, just print name
            return '_'+data[1]
        elif data[0] == const.TYPE_VAR:
            # quantity
            return str(data[1])
        elif data[0] == const.TYPE_SYM:
            # symbol
            return data[1].name
        elif data[0] == const.TYPE_FUNC:
            formatted_args = []
            for arg in data[2]:
                formatted_args.append(self.dataString(arg))
            return data[1]+'('+','.join(formatted_args)+')'
        elif data[0] == const.TYPE_ARITH:
            # arithmetic
            # Need to add parentheses, but let's do it sparingly...
            # We only need them when we exponentiate or when the arguments are add/subtract
            A = data[2]
            B = data[3]
            
            # Recurse
            AString = self.dataString(A)
            BString = self.dataString(B)
            
            # If we are exponentiating then we'll add parentheses for arithmetic arguments
            if data[1] not in (const.OP_ADD, const.OP_SUB):
                # don't add parentheses for simple add/subtract ever
                if A[0] == const.TYPE_ARITH and (A[1] in [const.OP_ADD, const.OP_SUB] or data[1] == const.OP_EXP):
                    AString = '('+AString+')'
                if B[0] == const.TYPE_ARITH and (B[1] in [const.OP_ADD, const.OP_SUB] or data[1] == const.OP_EXP):
                    BString = '('+BString+')'
            
            return AString+const.OP_CHAR[data[1]]+BString

def combine_args(*ARGS):
    """
    Combine arguments from multiple functions, sorting appropriately and removing dupes.
    """
    UNSRT = {}
    for arglist in ARGS:
        # throw these arguments in
        for arg in arglist:
            if arg.name in UNSRT.keys():
                # this argument exists, check if dimensions match and ignore
                if arg.dimensions != UNSRT[arg.name].dimensions:
                    raise exc('Two symbols with different dimensions may not use same name.')
            else:
                UNSRT[arg.name] = arg
    # have to get keys out then sort them
    KEYS = UNSRT.keys()
    KEYS.sort()
    # now go through and add to list in order
    OUT = []
    for key in KEYS:
        OUT.append(UNSRT[key])
    
    return tuple(OUT)  # remove dupes last (might be stupid)
    
def evaluate(string):
    """
    Evaluate a string of puny code.

    This will evaluate any single puny command. It does not handle any flow control code, nor
    does it split code on semicolons. It will, however recursively compute a __SINGLE__ puny command,
    i.e. "34.5<J>*(b/2)|in N"

    Throws puny.exc on error

    @param string puny statement string

    @return quantity resulting from statement
    """
    global CONSTANTS, WORKSPACES, DEFAULTWS

    string = string.strip()

    if string == '':
        raise exc('Cannot evaluate empty command')

    postcommatch = re.search('(?P<statement>.*)\|\s*(?P<pc>\S+)\s+(?P<pcargs>[^()]*)$', string)
    if postcommatch != None:
        # looks like a postcommand is attached
        pc = postcommatch.group('pc')

        OUT = evaluate(postcommatch.group('statement'))

        # POSTCOMMANDS
        if pc == 'in':
            # want to convert units to something
            A = quantity()
            A.unitsFromString(postcommatch.group('pcargs'))
            OUT.convert(A.units)
            del A

            return OUT
        elif pc == 'fu':
            # forcing units onto something
            OUT.unitsFromString(postcommatch.group('pcargs'))

            return OUT
        else:
            # unknown postcommand
            del OUT
            raise exc('Unknown postcommand: ' + pc)

    if string[0] == '_' and string[1:] in CONSTANTS.keys():
        # this is the name of a physical constant i.e. _c or _hbar
        return copy.deepcopy(CONSTANTS[string[1:]])

    # Check for existing variable, symbol, or function
    wsvarname = string.split('.',1)
    if len(wsvarname) == 2:
        # possible variable reference by workspace
        (ws, varname) = wsvarname
        ws = ws.upper()
    else:
        ws = DEFAULTWS

    if ws in WORKSPACES.keys():    
        for holder in [WORKSPACES[ws].variables, WORKSPACES[ws].symbols, WORKSPACES[ws].functions]:
            if string in holder.keys():
                # is this a variable in the current workspace??
                return copy.deepcopy(holder[string])

    OUT = quantity()
    if OUT.translateString(string):
        # string is just a bare quantity (i.e. 25<cm>)
        return OUT
    else:
        # clean this up if it doesn't pan out
        del OUT

    # check for operators outside parens (binomials, fractions, exponentials...)
    PAREN_LEVEL = 0     # start at outside
    IN_UNIT = False     # are we inside carats?
    IN_VECTOR = 0       # inside brackets/incremental vector declaration?

    for binop in ['+', '-', '*', '/', '^']:
        # loop through binary operations
        for i in range(len(string)-1, -1, -1):
            # traverse string and check each character
            if string[i] == '(':
                PAREN_LEVEL -= 1

            elif string[i] == ')':
                PAREN_LEVEL += 1

            elif string[i] == '<':
                IN_UNIT = False

            elif string[i] == '>':
                IN_UNIT = True

            elif string[i] == '[':
                IN_VECTOR -= 1

            elif string[i] == ']':
                IN_VECTOR += 1

            if PAREN_LEVEL == 0 and \
                        IN_VECTOR == 0 and \
                        not IN_UNIT and \
                        not (i > 1 and (string[i-1] == 'e' or string[i-1] == 'E') and re.match('[0-9\.]', string[i-2])) and  \
                        not ((binop == '+' or binop == '-') and i > 0 and re.search('\^\s*$', string[:i])) and \
                        string[i] == binop:
                # execute this binary operation

                if re.match('^\s*$', string[:i]) != None and \
                        (binop == '+' or \
                        binop == '-'):

                    # blank first part. negation or positive (i.e.  +231, -12)

                    # if this subevaluation raises an error then it will be caught in a higher level
                    B = evaluate(string[i + 1:])
                    A = quantity(values=[0], error=[0], units=B.units, dimensions=B.dimensions)

                    OPOUT = copy.deepcopy(B)
                    for i in range(len(OPOUT.values)):
                        OPOUT.values[i] = eval(binop + str(OPOUT.values[i]))

                    return OPOUT

                else:
                    A = evaluate(string[:i])
                    B = evaluate(string[i+1:])

                    if binop == '^':
                        # we use ^ instead of **, so can't just straight up eval the thing
                        OPOUT = A ** B
                    else:
                        OPOUT = eval('A ' + binop + ' B')

                    return OPOUT

        if IN_UNIT:
            raise exc("Unmatched units bracket!")

        if PAREN_LEVEL != 0:
            # unmatched parenthesis
            raise exc("Unmatched parenthesis!")

    if string[0] == '(':
        # command is encapsulated by parentheses
        return evaluate(string[1:-1])    # break out of parentheses

    funcmatch = re.search('(?P<function>\w+)\((?P<args>.*)\)', string)
    if funcmatch != None:
        # looks like a function call
        funcname = funcmatch.group('function')
        argstr = funcmatch.group('args')

        ARGS = copy.deepcopy([])
        PAREN_LEVEL = 0
        BRACKET_LEVEL = 0
        TMP_CMD = ''
        if not re.match('\s*$', argstr):
            for i in range(len(argstr)+1):
                if i == len(argstr) or \
                        (argstr[i] == ',' and \
                        PAREN_LEVEL == 0 and \
                        BRACKET_LEVEL == 0):
                    ARGS.append(TMP_CMD)   # add to array

                    TMP_CMD = ''

                else:
                    TMP_CMD += argstr[i]
                    if argstr[i] == '(':
                        PAREN_LEVEL += 1

                    elif argstr[i] == ')':
                        PAREN_LEVEL -= 1

                    elif argstr[i] == '[':
                        BRACKET_LEVEL += 1

                    elif argstr[i] == ']':
                        BRACKET_LEVEL -= 1


        if funcname in pmath.FUNCTIONS.keys():
            # pmath function
            return apply(pmath.FUNCTIONS[funcname].__call__, ARGS)

        elif funcname in WORKSPACES[DEFAULTWS].functions.keys():
            # user-defined function included in workspace
            return WORKSPACES[DEFAULTWS].functions[funcname](ARGS)

    raise exc("Could not interpret '" + string + "'!")

def careful_split(delim, string):
    """
    Carefully split a string, without breaking parentheses, carats, strings, brackets, etc...

    @param delim delimiter to split on (passing None will split on whitespace)
    @param str string to be split

    @return list of all nonempty split objects
    """
    if delim != None and delim in ['(', ')', '[', ']', '<', '>', "'"] or delim == '':
        raise exc("invalid delimiter: "+delim)

    WS_SPLIT = delim == None    # are we splitting on whitespace or not?

    OUTPUT = []
    tmpstr = ''

    PAREN_LEVEL = 0     # start at outside
    IN_UNIT = False     # are we inside carats?
    IN_VECTOR = 0       # inside brackets/incremental vector declaration?
    IN_STRING = False
    for i in range(len(string)):
        # traverse string and check each character
        if string[i] == '(':
            PAREN_LEVEL -= 1

        elif string[i] == ')':
            PAREN_LEVEL += 1

        elif string[i] == '<':
            IN_UNIT = True

        elif string[i] == '>':
            IN_UNIT = False

        elif string[i] == '[':
            IN_VECTOR -= 1

        elif string[i] == ']':
            IN_VECTOR += 1

        elif string[i] == "'":
            # get in or out of string
            IN_STRING = not IN_STRING

        if (WS_SPLIT and re.match('\s', string[i])) or \
            (string[i] == delim and IN_VECTOR == 0 and not IN_UNIT \
            and not IN_STRING and PAREN_LEVEL == 0):
            # split
            if tmpstr != '':
                # append only nonempty strings
                OUTPUT.append(tmpstr)
                tmpstr = ''

        else:
            tmpstr += string[i]

    if tmpstr != '':
        # append last entry unless string ends in delimiter
        OUTPUT.append(tmpstr)
        tmpstr = ''

    return OUTPUT

# let's create an infinity
inf = 1.0e500

# calculate precision of floats
eps = 1.
while (eps/2. + 1.) > 1.:
    eps = eps / 2.

# TODO: configuration of default error. 0 or machine limit?
DEFAULT_ERROR = 0

# Dimension definitions
# These are the only dimensions currently handled by the module
DIMENSIONS = {}
DIMENSIONS[1] = ['L', 'Length']
DIMENSIONS[2] = ['M', 'Mass']
DIMENSIONS[3] = ['t', 'Time']
DIMENSIONS[4] = ['T', 'Temperature']
DIMENSIONS[5] = ['I', 'Current']
DIMENSIONS[6] = ['LI', 'Luminous Intensity']

# Load units database
unitsfp = open(punyDir+'units.db.pickle', 'rb')
UNITS = cPickle.load(unitsfp)
unitsfp.close()

# Load constants database
cfp = open(punyDir+'constants.db.pickle', 'rb')
CONSTANTS = cPickle.load(cfp)
cfp.close()

# have to start up with at least one default workspace
DEFAULTWS = 'DEFAULT'
WORKSPACES = {DEFAULTWS : workspace()}
