#!/usr/bin/env python
"""
    compile_data - compile builtin functions and keywords
"""

import marshal, cPickle, re, os

import puny, pmath, pkwstd

def compile_functions():
    """
    Build pre-compiled function files in punyDir/functions/
    """
    
    # create and marshal FUNCTIONS dict first
    print "Compiling function index file..."
    
    FUNCTIONS = {}
    
    commentmatch = re.compile('(?P<content>[^#%]*)(\s*[#%]\s*)?(?P<comment>.+)?')
    linematch = re.compile( '(?P<name>.*)\t'+
                            '(?P<minargs>.*)\t'+
                            '(?P<maxargs>.*)\t'+
                            '(?P<stringargs>.*)\t'+
                            '(?P<usage>.*)\t'+
                            '(?P<desc>.*)')
    for file in os.listdir(puny.punyDir+'functions'):
        if os.path.isdir(puny.punyDir+'functions/'+file):
            try:
                indexfp = open(puny.punyDir+'functions/'+file+'/package.index', 'r')
                
                print "  Package: "+file
                for line in indexfp.readlines():
                    cm = commentmatch.match(line)
                    content = cm.group('content')   # strip comments
                    m = linematch.match(content)
                    if m:
                        name = m.group('name')
                        minargs = int(m.group('minargs'))
                        maxargs = int(m.group('maxargs'))
                        stringargstr = m.group('stringargs')
                        usage = m.group('usage')
                        desc = m.group('desc')
                        
                        stringargs = []
                        for arg in stringargstr.split(','):
                            if not arg == stringargstr and not arg == '':
                                stringargs.append(int(arg))
                        
                        FUNCTIONS[name] = pmath.function(name=name, \
                                minargs=minargs, \
                                maxargs=maxargs, \
                                stringargs=stringargs, \
                                usage=usage, \
                                description=desc,
                                package=file)
                        
                indexfp.close()
            
            except IOError:
                print "Skipping package directory "+file+": Index file not found or insufficient permissions."
    
    mainindexfp = open(puny.punyDir+'functions/index.pickle', 'wb')
    cPickle.dump(FUNCTIONS, mainindexfp)
    mainindexfp.close()
    
    for func in FUNCTIONS.keys():
        # cycle through function names
        try:
            print puny.punyDir+'functions/'+FUNCTIONS[func].package+'/'+func+".f.py"
            textfp = open(puny.punyDir+'functions/'+FUNCTIONS[func].package+'/'+func+".f.py", 'r')
            code = textfp.read()
            textfp.close()
        except IOError:
            raise puny.exc('Could not open function file '+FUNCTIONS[func].package+'/'+func+'.f.py for compilation')
        
        try:
            compiled = compile(code, puny.punyDir + "functions/" + FUNCTIONS[func].package+'/'+func + ".f.py", 'exec')
        except:
            print "ERROR: function " + func + " could not be compiled"
            continue
        
        try:
            outfp = open(puny.punyDir + "functions/" + FUNCTIONS[func].package+'/'+func + ".fc", 'wb')
            
            marshal.dump(compiled, outfp)
            
            outfp.close()
        except IOError:
            raise puny.exc('Could not open function code file for writing')

def compile_keywords():
    """
    Build pre-compiled function files in ${punyDir}/functions/
    """
    for kw in pkwstd.KEYWORDS.keys():
        # cycle through keyword names
        try:
            print puny.punyDir + "keywords/"+kw+".k.py"
            textfp = open(puny.punyDir + "keywords/"+kw+".k.py", 'r')
            code = textfp.read()
            textfp.close()
        except IOError:
            raise puny.exc('Could not open keyword file '+kw+'.k.py for compilation')
        
        try:
            compiled = compile(code, puny.punyDir + "keywords/" + kw + ".k.py", 'exec')
        except:
            print "ERROR: keyword " + kw + " could not be compiled"
            continue
        
        try:
            outfp = open(puny.punyDir + "keywords/" + kw + ".kc", 'wb')
            
            marshal.dump(compiled, outfp)
            
            outfp.close()
        except IOError:
            raise puny.exc('Could not open keyword code file for writing')

def marshal_units():
    """
    Marshal the UNITS dict for faster loading
    """
    UNITS = {}
    # TODO: Allow base unit to be name of unit instead of numer, then can order in a sane way
    dbfp = open(puny.punyDir+'units.db','r')
    lineno = 0
    
    commentmatch = re.compile('(?P<content>[^#%]*)(\s*[#%]\s*)?(?P<comment>.+)?')
    linematch = re.compile( '(?P<num>.*)\t'+
                            '(?P<name>.*)\t'+
                            '(?P<abbrev>.*)\t'+
                            '(?P<aliases>.*)\t'+
                            '(?P<dim>.*)\t'+
                            '(?P<BU>.*)\t'+
                            '(?P<BD>.*)\t'+
                            '(?P<TB>.*)\t'+
                            '(?P<TBO>.*)\t'+
                            '(?P<FB>.*)\t'+
                            '(?P<FBO>.*)')
    for line in dbfp.readlines():
        lineno = lineno + 1
        
        cm = commentmatch.match(line)
        content = cm.group('content')   # strip comments
        m = linematch.match(content)
        if m:
            num = int(m.group('num'))
            name = m.group('name')
            abbrev = m.group('abbrev')
            aliasstr = m.group('aliases')
            dimstr = m.group('dim')
            BU = int(m.group('BU'))
            BD = int(m.group('BD'))
            TB = float(m.group('TB'))
            TBO = float(m.group('TBO'))
            FB = float(m.group('FB'))
            FBO = float(m.group('FBO'))
            
            aliases = aliasstr.split(',')
            if len(aliases) == 1 and aliases[0] == "":
              aliases = [];
            dimensions = puny.dimension_dict()
            for dim in dimstr.split(','):
                if not dim == '':
                    # if it's an empty string don't try to find any elements
                    (key,val) = dim.split(':')
                    dimensions[int(key)] = int(val)
                
            UNITS[num] = puny.unit( name = name,
                                    abbrev = abbrev,
                                    aliases = aliases,
                                    dimensions = dimensions,
                                    base_unit = BU,
                                    base_dimension = BD,
                                    to_base = TB,
                                    to_base_offset = TBO,
                                    from_base = FB,
                                    from_base_offset = FBO)
                                    
    dbfp.close()
    
    pfp = open(puny.punyDir+'units.db.pickle','wb')
    cPickle.dump(UNITS, pfp)
    pfp.close()

def marshal_constants():
    """
    Marshal the CONSTANTS dict for faster loading
    """
    CONSTANTS = {}
    
    dbfp = open(puny.punyDir+'constants.db','r')
    lineno = 0
    
    commentmatch = re.compile('(?P<content>[^#%]*)(\s*[#%]\s*)?(?P<comment>.+)?')
    linematch = re.compile( '(?P<abbrev>.*)\t'+
                            '(?P<name>.*)\t'+
                            '(?P<value>.*)\t'+
                            '(?P<error>.*)\t'+
                            '(?P<units>.*)\t'+
                            '(?P<dim>.*)')
    for line in dbfp.readlines():
        lineno = lineno + 1
        
        cm = commentmatch.match(line)
        content = cm.group('content')   # strip comments
        m = linematch.match(content)
        if m:
            abbrev = m.group('abbrev')
            name = m.group('name')
            valuestr = m.group('value')
            errorstr = m.group('error')
            unitstr = m.group('units')
            dimstr = m.group('dim')
            
            values = []
            for val in valuestr.split(','):
                values.append(float(val))
            error = []
            for err in errorstr.split(','):
                error.append(float(err))
            
            units = {}
            for unit in unitstr.split(','):
                if not unit == '':
                    # if it's an empty string don't try to find any elements
                    (key,val) = unit.split(':')
                    units[int(key)] = int(val)
            
            dimensions = puny.dimension_dict()
            for dim in dimstr.split(','):
                if not dim == '':
                    # if it's an empty string don't try to find any elements
                    (key,val) = dim.split(':')
                    dimensions[int(key)] = int(val)
            
            CONSTANTS[abbrev] = puny.quantity( name = '',
                                    values = values,
                                    error = error,
                                    units = units,
                                    dimensions = dimensions)
            # can't set name above because it inherently uses the setName method
            CONSTANTS[abbrev].name = name # force wierd looking names

    dbfp.close()
    
    pfp = open(puny.punyDir+'constants.db.pickle','wb')
    cPickle.dump(CONSTANTS, pfp)
    pfp.close()
    
if __name__ == "__main__":
    # main loop
    print "Compiling functions..."
    compile_functions()
    
    print "\nCompiling keywords..."
    compile_keywords()
    
    print "\nSerializing units..."
    marshal_units()
    
    print "\nSerializing constants..."
    marshal_constants()
    
