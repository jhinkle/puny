if len(ARGS[0].units) != 0:
    raise puny.exc("sine function requires unit-less parameter")
    
else:
    # output is unit-less, PURE number
    for i in range(len(ARGS[0].values)):
        OUT.values.append(math.sin(ARGS[0].values[i]))
            # dC = cos(A)*dA
        OUT.error.append(abs(math.cos(ARGS[0].values[i]))*ARGS[0].error[i])

    OUT.units = {}
    OUT.dimensions = {}
