hmsmatch = re.match('\s*(?P<hour>[\de\. ]+\s*h)?\s*(?P<min>[\de\. ]+\s*m)?\s*(?P<sec>[\de\. ]+\s*s)?\s*$', ARGS[0], re.I)
dmsmatch = re.match('\s*(?P<deg>[\d\.\+\-e]+)?\s*(?P<min>[\d\.\+\-e]+\s*\')?\s*(?P<sec>[\d\.\+\-e]+\s*\")?\s*$', ARGS[0], re.I)

if hmsmatch != None:
    # hour minute second - astronomy right ascension
    
    HOUR = hmsmatch.group('hour')
    MINS = hmsmatch.group('min')
    SECS = hmsmatch.group('sec')
    
    if HOUR == None:
        HOUR = 0
    else:
        HOUR = float(re.sub('\s*h$', '', HOUR))
        
    if MINS == None:
        MINS = 0
    else:
        MINS = float(re.sub('\s*m$', '', MINS))

    
    if SECS == None:
        SECS = 0
    else:
        SECS = float(re.sub('\s*s$', '', SECS))

    OUT.values.append(2*math.pi*(HOUR/24 + MINS/1440 + SECS/86400))
    OUT.error.append(0)   # exact output

elif dmsmatch != None:
    # degrees-minutes-seconds (DMS)

    DEGS = dmsmatch.group('deg')
    MINS = dmsmatch.group('min')
    SECS = dmsmatch.group('sec')

    if DEGS == None:
        DEGS = 0
    else:
        DEGS = float(DEGS)
    
    if MINS == None:
        MINS = 0
    else:
        MINS = float(re.sub('\s*\'$', '', MINS))
    
    if SECS == None:
        SECS = 0
    else:
        SECS = float(re.sub('\s*"$', '', SECS))

    if DEGS >= 0:
        SIGN = 1
    else:
        SIGN = -1
    
    OUT.values.append(SIGN*math.pi*(abs(DEGS)/180 + MINS/10800 + SECS/648000))
    OUT.error.append(0)  # exact output

else:
    raise puny.exc('String is not in a recognized angle format.')
