Returns the tangent of angle theta.  theta must be dimension-less and unit-less (radians). When using degrees or some other angle system, the rad() function may be used to convert to radians.
