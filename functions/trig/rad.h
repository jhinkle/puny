This function takes a string in any of the following formats:

Degrees/Minutes/Seconds (DMS): <degrees> <arcmin>' <arcsec>"
Equatorial Coordinate System (right ascension): <hours>h <minutes>m <seconds>s

The function returns a unit-less number, which is the angle converted to radians.
