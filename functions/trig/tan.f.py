if len(ARGS[0].units) != 0:
    raise puny.exc("tangent function requires unit-less parameter")
    
else:
    # output is unit-less, PURE number
    for i in range(len(ARGS[0].values)):
        OUT.values.append(math.tan(ARGS[0].values[i]))
            # D(tan(A)) = dA * (sec(A))^2 = dA / (cos(A))^2
        OUT.error.append(ARGS[0].error[i]/((cos(ARGS[0].values[i])**2)))

    OUT.units = {}
    OUT.dimensions = {}
