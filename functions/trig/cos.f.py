if len(ARGS[0].units) != 0:
    raise puny.exc("cosine function requires unit-less parameter")
    
else:
    # output is unit-less, PURE number
    for i in range(len(ARGS[0].values)):
        OUT.values.append(math.cos(ARGS[0].values[i]))
            # dC = sin(A)*dA
        OUT.error.append(abs(math.sin(ARGS[0].values[i]))*ARGS[0].error[i])

    OUT.units = {}
    OUT.dimensions = {}
