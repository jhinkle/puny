if len(ARGS[0].units) != 0:
    output("Logarithm function requires unit-less parameter.")
    
else:
    #output unit-less, PURE number
    LOG10 = math.log(10) # ln(10)

    for i in range(len(ARGS[0].values)):
        OUT.values.append(math.log10(ARGS[0].values[i]))
            # D(log10(A)) = dA / A / ln(10)
        OUT.error.append(ARGS[0].error[i] / (ARGS[0].values[i] * LOG10))

