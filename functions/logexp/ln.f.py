if len(ARGS[0].units) != 0:
    raise puny.exc("natural logarithm function requires unit-less parameter")

else:
    OUT.units = {}
    OUT.dimensions = {}

    # output unit-less, PURE number
    for i in range(len(ARGS[0].values)):            
        if ARGS[0].values[i] <= 0:
            raise puny.exc('logarithm only defined for positive numbers')

        else:
            # regular number
            OUT.values.append(math.log(ARGS[0].values[i]))
                # D(ln(A)) = dA / abs(A)
            OUT.error.append(ARGS[0].error[i] / abs(ARGS[0].values[i]))
