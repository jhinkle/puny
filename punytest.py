
"""

    punytest - puny unit testing suite
    Copyright (C) 2003  Jacob Hinkle

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

import unittest, puny, math

class ScalarAssignmentTest(unittest.TestCase):
    def testString(self):
        A = puny.quantity(string='  34.32e34 ')
        assert(len(A.values) == 1)
        assert(len(A.error) == 1)
        assert(A.values == [34.32e34])
        assert(A.error == [puny.eps * 10**math.floor(math.log10(A.values[0]))])
        
    def testStringError(self):
        A = puny.quantity(string=' 34 ( .023 ) ')
        assert(len(A.values) == 1)
        assert(len(A.error) == 1)
        assert(A.values == [34])
        assert(A.error == [.023])
    
    def testStringUnits(self):
        A = puny.quantity(string=' 34 <J m^-2 A^3.5> ')
        assert(len(A.values) == 1)
        assert(len(A.error) == 1)
        assert(A.values == [34])
        assert(A.error == [puny.eps * 10**math.floor(math.log10(A.values[0]))])
        # translateString should NOT combine any given units!
        assert(A.units == {7:1, 1:-2., 10:3.5})
        assert(A.dimensions == {2:1, 3:-2, 5:3.5})
        
    def testStringUnitsError(self):
        A = puny.quantity(string=' 34  ( .23e-2 )  <J m^-2 A^3.5> ')
        assert(len(A.values) == 1)
        assert(len(A.error) == 1)
        assert(A.values == [34])
        assert(A.error == [.0023])
        # translateString should NOT combine any given units!
        assert(A.units == {7:1, 1:-2., 10:3.5})
        assert(A.dimensions == {2:1, 3:-2, 5:3.5})
        
    def testValueError(self):
        A = puny.quantity(values=[342], error=[.03])
        assert(A.values == [342])
        assert(A.error == [.03])
        
    def testValueWrongError(self):
        try:
            A = puny.quantity(values=[342], error=[.03, 0.1])
        except puny.exc, str:
            return
        
        raise puny.exc('Incorrect error allowed!')
        
    def testValueNoError(self):
        A = puny.quantity(values=[1.20])
        assert(A.values == [1.20])
        assert(A.error == [puny.eps * 1.2])
                
    def testExplicitUnitsNoDims(self):
        A = puny.quantity(values=[1.20], error=[.1], units={2:1, 3:-2, 44:1})
        assert(A.values == [1.20])
        assert(A.error == [.1])
        assert(A.units == {2:1, 3:-2, 44:1})
        assert(A.dimensions == {3:2, 1:-2, 5:2})
        
    def testExplicitUnitsDims(self):
        pass
        
    def testExplicitUnitsWrongDims(self):
        pass

class VectorAssignmentTest(unittest.TestCase):
    def testString(self):
        A = puny.quantity(string='[23, 120.3, 456]')
        
    def testStringError(self):
        A = puny.quantity(string='[45, 202.2, 13.4e-24](.01)')
    
    def testStringUnits(self):
        pass
        
    def testStringUnitsError(self):
        pass
        
    def testValueError(self):
        pass
        
    def testValueWrongError(self):
        pass
        
    def testValueNoError(self):
        pass
                
    def testExplicitUnitsNoDims(self):
        pass
        
    def testExplicitUnitsDims(self):
        pass
        
    def testExplicitUnitsWrongDims(self):
        pass

class ConversionTest(unittest.TestCase):
    pass

class ReprTest(unittest.TestCase):
    def testRepr(self):
        pass

class CopyTest(unittest.TestCase):
    def testCopy(self):
        pass
        
if __name__ == '__main__':
    unittest.main()
