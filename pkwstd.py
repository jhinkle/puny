
"""

    pkwstd - Puny standard keyword module
    Copyright (C) 2003  Jacob Hinkle

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

import re
import marshal
import types

import puny, pmath

# IMPORTANT!!!
# override this whenever importing this module, or you will receive errors
# should set this to some function which takes a single argument (a string)
output = None

class keyword:
    """
    Base keyword class. Holds description, usage, help, minargs, and code.
    """
    def __init__(self, name=None, description=None, usage=None, minargs=0):
        """
        Constructor.

        @param name name of keyword
        @param description description of what the keyword does
        @param usage explanation of basic usage
        @param minargs minimum number of arguments (split on whitespace)
        """
        self.name = name
        self.description = description
        self.usage = usage
        self.minargs = minargs
        self.bytecode = None

    def execute(self, ARGS, ARGSTRING):
        """
        Execute keyword.

        @param args argument list to be passed to keyword
        """
        global output

        if self.bytecode == None:
            # haven't loaded this keyword into memory yet

            try:
                compfp = open(puny.punyDir + "keywords/" + self.name + ".kc", 'rb')

                # load compiled code from file
                self.bytecode = marshal.load(compfp)

                compfp.close()
            except IOError:
                # else load and compile code from text file ... + ".f.py"
                output('Warning: Compiled keyword '+self.name+' not found. Attempting to parse uncompiled code.')
                
                try:
                    textfp = open(puny.punyDir + "keywords/" + self.name + ".k.py", 'r')
                    code = textfp.read()
                    textfp.close()
                except IOError:
                    raise puny.exc('Could not open keyword file: '+self.name+'.k.py (or corresponding *.kc file)')

                self.bytecode = compile(code, puny.punyDir + "keywords/" + self.name + ".k.py", 'exec')

        # we have compiled code so execute it
        exec(self.bytecode)

def exec_keyword(kw, argstr):
    """
    Executes a requested keyword kw with argument string argstr

    @param kw name of the keyword to execute
    @param argstr argument string following keyword

    @return True for success or False on unknown keyword
    """
    global KEYWORDS, EXTERNAL_KEYWORDS

    if kw == '':
        # they're asking for a list of keywords

        return KEYWORDS.keys() + EXTERNAL_KEYWORDS.keys()

    elif kw in KEYWORDS.keys():
        # known keyword

        # parse arguments (split on whitespace, respecting quotes)
        argstrings = puny.careful_split(None, argstr)

        if len(argstrings) < KEYWORDS[kw].minargs:
            raise puny.exc(KEYWORDS[kw].name+": minimum "+str(KEYWORDS[kw].minargs)+" arguments required")

        ARGS = []
        for string in argstrings:
            try:
                ARGS.append(puny.evaluate(string))
            except puny.exc:
                # doesn't look like a quantity, so pass it as a string
                ARGS.append(string)

        KEYWORDS[kw].execute(ARGS, argstr)

        return True

    else:
        return False


KEYWORDS = {} # load keywords
try:
  for line in open(puny.punyDir + "keywords/index",'r'):
    if not re.match("\s*\#", line):
      fields = line.split(',')
      if len(fields) == 4:
        (name,desc,usage,minargs) = fields
        KEYWORDS[name] = keyword(name,desc,usage,int(minargs))
except IOError:
  raise puny.exc('Could not read keyword index file: '+puny.punyDir + 'keywords/index')
  
EXTERNAL_KEYWORDS = {}
EXTERNAL_KEYWORD_DIR = None
